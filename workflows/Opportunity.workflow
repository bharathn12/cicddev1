<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Agency_Partnership_Big_Deal_Notification</fullName>
        <description>Agency Partnership Big Deal Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Agency_Partnerships_Big_Deal_Alert</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Agency_Big_Deal_Alert</template>
    </alerts>
    <alerts>
        <fullName>Corporate_Big_Deal_Notification</fullName>
        <description>Corporate Big Deal Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Business_Government_Big_Deal_Alert</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Corporate_Big_Deal_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Auto_update_category_to_QBS_for_amount</fullName>
        <description>If Category is CA and Proposed Revenue amount is less than 300K then Category defaults to QBS</description>
        <field>Category__c</field>
        <literalValue>Qantas Business Savings</literalValue>
        <name>Auto update category to QBS for amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Update_Counter</fullName>
        <description>To determine how many times an opportunity gets updated post closure for B&amp;G Opportunities and post submission for SME product &gt;&gt; AEQCC partner plus.</description>
        <field>Opp_update_counter__c</field>
        <formula>IF(((Opp_update_counter__c &gt;= 0) &amp;&amp; ( ISPICKVAL( Type , &apos;SME Product&apos;)) &amp;&amp; (ISPICKVAL( Sub_Type_Level_2__c , &apos;Partner Plus&apos;))&amp;&amp; (ISPICKVAL( Sub_Type_Level_3__c , &apos;Submitted&apos;))) 
||((Opp_update_counter__c &gt;= 0) &amp;&amp;( ISPICKVAL( Type , &apos;Business and Government&apos;)) &amp;&amp; (ISPICKVAL( StageName , &apos;Closed Accepted&apos;))) , Opp_update_counter__c + 1, 0)</formula>
        <name>Opp Update Counter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Stage_Update</fullName>
        <description>Whenever the Number of Proposals is 1 the Stage field is updates to Propose</description>
        <field>StageName</field>
        <literalValue>Propose &amp; Negotiate</literalValue>
        <name>Opportunity Stage Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SME_Product</fullName>
        <description>Set Opportunity record type to SME Product</description>
        <field>RecordTypeId</field>
        <lookupValue>SME_Product</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opp recType SME Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_recType_Agency_Partnerships</fullName>
        <description>Set Opportunity record type to  Agency Partnerships</description>
        <field>RecordTypeId</field>
        <lookupValue>Agency_Partnerships</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opp recType Agency Partnerships</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_recType_Business_and_Government</fullName>
        <description>Set Opportunity record type to Business and Government</description>
        <field>RecordTypeId</field>
        <lookupValue>Business_and_Government</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opp recType Business and Government</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_recType_GSA</fullName>
        <description>Set Opportunity record type to GSA</description>
        <field>RecordTypeId</field>
        <lookupValue>GSA</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opp recType GSA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_recType_Sales_Development</fullName>
        <description>Set Opportunity record type to Sales Development</description>
        <field>RecordTypeId</field>
        <lookupValue>Sales_Development</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opp recType Sales Development</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_Agency_Partnerships</fullName>
        <description>Set Opportunity record type to  Agency Partnerships</description>
        <field>RecordTypeId</field>
        <lookupValue>Agency_Partnerships</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type &apos;Agency Partnerships&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_Business_and_Government</fullName>
        <description>Set Opportunity record type to Business and Government</description>
        <field>RecordTypeId</field>
        <lookupValue>Business_and_Government</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type &apos;Business and Government</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_Charters</fullName>
        <description>Set Opportunity record type to Charter</description>
        <field>RecordTypeId</field>
        <lookupValue>Charters</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type &apos;Charters&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_GSA</fullName>
        <description>Set Opportunity record type to GSA</description>
        <field>RecordTypeId</field>
        <lookupValue>GSA</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type &apos;GSA&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_Global_Opportunity</fullName>
        <description>Set Opportunity record type to Global Opportunity</description>
        <field>RecordTypeId</field>
        <lookupValue>Global_opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type &apos;Global Opportunity&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_SME_Product</fullName>
        <description>Set Opportunity record type to SME Product</description>
        <field>RecordTypeId</field>
        <lookupValue>SME_Product</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type &apos;SME Product&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_Sales_Development</fullName>
        <description>Set Opportunity record type to Sales Development</description>
        <field>RecordTypeId</field>
        <lookupValue>Sales_Development</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type &apos;Sales Development&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_type_to_Agency_Partnerships</fullName>
        <description>Set Opportunity type to Charters</description>
        <field>Type</field>
        <literalValue>Agency Partnerships</literalValue>
        <name>Set type to Agency Partnerships</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_type_to_Business_and_Government</fullName>
        <description>Set Opportunity type to Business and Government</description>
        <field>Type</field>
        <literalValue>Business and Government</literalValue>
        <name>Set type to Business and Government</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_type_to_Charters</fullName>
        <description>Set Opportunity type to Charters</description>
        <field>Type</field>
        <literalValue>Charters</literalValue>
        <name>Set type to Charters</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_type_to_GSA</fullName>
        <description>Set Opportunity type to GSA</description>
        <field>Type</field>
        <literalValue>GSA</literalValue>
        <name>Set type to GSA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_type_to_SME_Product</fullName>
        <description>Set Opportunity type to SME Product</description>
        <field>Type</field>
        <literalValue>SME Product</literalValue>
        <name>Set type to SME Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_type_to_Sales_Development</fullName>
        <description>Set Opportunity type to Sales Development</description>
        <field>Type</field>
        <literalValue>Sales Development</literalValue>
        <name>Set type to Sales Development</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_the_Future_Planet_Checkbox</fullName>
        <description>Tick the Future Planet Checkbox</description>
        <field>Future_Planet__c</field>
        <literalValue>1</literalValue>
        <name>Tick the Future Planet Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Proposed_Revenue_Amount</fullName>
        <field>Amount</field>
        <formula>Forecast_Charter_Spend__c</formula>
        <name>Update Proposed Revenue Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Auto Populate Forecast Charter Spend to Proposed Revenue Amount</fullName>
        <actions>
            <name>Update_Proposed_Revenue_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Charters</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Auto update category to QBS for amount less than 300K</fullName>
        <actions>
            <name>Auto_update_category_to_QBS_for_amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>lessThan</operation>
            <value>&quot;AUD 300,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Category__c</field>
            <operation>equals</operation>
            <value>Corporate Airfares</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>System Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Adhoc Charter,MICE,Agency Partnerships,Business and Government,GSA,SME Product,Sales Development,Charters,Future Planet,Global opportunity,Group Sales,B&amp;G Campaigns</value>
        </criteriaItems>
        <description>If Category is CA and Proposed Revenue amount is less than 300K then Category defaults to QBS</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Big Deal Alert for Agency Partnership Opportunity</fullName>
        <actions>
            <name>Agency_Partnership_Big_Deal_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Agency Partnerships</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterOrEqual</operation>
            <value>AUD 10,AUD 0</value>
        </criteriaItems>
        <description>Will send a email notification you set of user once the Agency partnership opportunity proposed revenue is greater than equal to 10 M</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Big Deal Alert for Corporate Opportunity</fullName>
        <actions>
            <name>Corporate_Big_Deal_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Business and Government</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterOrEqual</operation>
            <value>AUD 10,AUD 0</value>
        </criteriaItems>
        <description>Will send a email notification you set of user once the corporate opportunity proposed revenue is greater than equal to 10 M</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MICE Opportunity</fullName>
        <actions>
            <name>ClosureDue_date</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>MICE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed - Accepted,Closed - Not Accepted,Closed - No Bid</value>
        </criteriaItems>
        <description>workflow task to notify the opportunity owner that the opportunity is due for closure (closure date – 11 months for the reminder).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp Update counter</fullName>
        <actions>
            <name>Opp_Update_Counter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>startsWith</operation>
            <value>QIS-A&amp;G</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>startsWith</operation>
            <value>QIS-R&amp;G</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>QIS-Acquisition and Growth</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Stage Update</fullName>
        <actions>
            <name>Opportunity_Stage_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Number_of_Proposals__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Adhoc Charter,MICE,Agency Partnerships,Business and Government,GSA,SME Product,Sales Development,Charters,Future Planet,Global opportunity,Group Sales,B&amp;G Campaigns</value>
        </criteriaItems>
        <description>Update stage on Opportunity when first Proposal is created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Tick Future Planet Checkbox when Future Planet Opportunity is Closed</fullName>
        <actions>
            <name>Tick_the_Future_Planet_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Future Planet</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed - Accepted</value>
        </criteriaItems>
        <description>Tick Future Planet Checkbox when Future Planet Opportunity is Closed</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>To update the record type to Agency Partnerships in Opportunity</fullName>
        <actions>
            <name>Set_Record_Type_Agency_Partnerships</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Agency Partnerships</value>
        </criteriaItems>
        <description>Sets Opportunity record type to &apos;Agency Partnerships&apos; when the Type field is changed to &apos;Agency Partnerships&apos; via another workflow.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>To update the record type to Business and Government in Opportunity</fullName>
        <actions>
            <name>Set_Record_Type_Business_and_Government</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Business and Government</value>
        </criteriaItems>
        <description>Sets Opportunity record type to &apos;Business and Government&apos; when the Type field is changed to &apos;Business and Government&apos; via another workflow.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>To update the record type to Charters in Opportunity</fullName>
        <actions>
            <name>Set_Record_Type_Charters</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Charters</value>
        </criteriaItems>
        <description>Sets Opportunity record type to &apos;Charters&apos; when the Type field is changed to &apos;Charters&apos; via another workflow.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>To update the record type to GSA in Opportunity</fullName>
        <actions>
            <name>Set_Record_Type_GSA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>GSA</value>
        </criteriaItems>
        <description>Sets Opportunity record type to &apos;SME Product&apos; when the Type field is changed to &apos;SME Product&apos; via another workflow.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>To update the record type to Global Opportunity</fullName>
        <actions>
            <name>Set_Record_Type_Global_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Global Opportunity</value>
        </criteriaItems>
        <description>Sets Opportunity record type to &apos;Global Opportunity&apos; when the Type field is set to &apos;Global Opportunity&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>To update the record type to SME Product in Opportunity</fullName>
        <actions>
            <name>Set_Record_Type_SME_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>SME Product</value>
        </criteriaItems>
        <description>Sets Opportunity record type to &apos;SME Product&apos; when the Type field is changed to &apos;SME Product&apos; via another workflow.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>To update the record type to Sales Development in Opportunity</fullName>
        <actions>
            <name>Set_Record_Type_Sales_Development</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Sales Development</value>
        </criteriaItems>
        <description>Sets Opportunity record type to &apos;Sales Development&apos; when the Type field is changed to &apos;Sales Development&apos; via another workflow.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>To update the type to Agency Partnerships in Opportunity</fullName>
        <actions>
            <name>Set_type_to_Agency_Partnerships</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type_System__c</field>
            <operation>equals</operation>
            <value>Agency Partnerships</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Adhoc Charter,MICE,Agency Partnerships,Business and Government,GSA,SME Product,Sales Development,Charters,Future Planet,Global opportunity,Group Sales,B&amp;G Campaigns</value>
        </criteriaItems>
        <description>When a Lead (with opportunity Type selected as &apos;Agency Partnerships&apos; is converted to opportunity this workflow will set the &apos;Type&apos; field in Opportunity to &apos;Agency Partnerships&apos;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>To update the type to Business and Government in Opportunity</fullName>
        <actions>
            <name>Set_type_to_Business_and_Government</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type_System__c</field>
            <operation>equals</operation>
            <value>Business and Government</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Adhoc Charter,MICE,Agency Partnerships,Business and Government,GSA,SME Product,Sales Development,Charters,Future Planet,Global opportunity,Group Sales,B&amp;G Campaigns</value>
        </criteriaItems>
        <description>When a Lead (with opportunity Type selected as &apos;Business and Government&apos;) is converted to opportunity this workflow will set the &apos;Type&apos; field in Opportunity to &apos;Business and Government&apos;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>To update the type to Charters in Opportunity</fullName>
        <actions>
            <name>Set_type_to_Charters</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type_System__c</field>
            <operation>equals</operation>
            <value>Charters</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Adhoc Charter,MICE,Agency Partnerships,Business and Government,GSA,SME Product,Sales Development,Charters,Future Planet,Group Sales,B&amp;G Campaigns</value>
        </criteriaItems>
        <description>When a Lead (with opportunity Type selected as &apos;Charters&apos;) is converted to opportunity this workflow will set the &apos;Type&apos; field in Opportunity to &apos;Charters&apos;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>To update the type to GSA in Opportunity</fullName>
        <actions>
            <name>Set_type_to_GSA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type_System__c</field>
            <operation>equals</operation>
            <value>GSA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Adhoc Charter,MICE,Agency Partnerships,Business and Government,GSA,SME Product,Sales Development,Charters,Future Planet,Global opportunity,Group Sales,B&amp;G Campaigns</value>
        </criteriaItems>
        <description>When a Lead (with opportunity Type selected as &apos;Sales Development&apos;) is converted to opportunity this workflow will set the &apos;Type&apos; field in Opportunity to &apos;Sales Development&apos;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>To update the type to SME Product in Opportunity</fullName>
        <actions>
            <name>Set_type_to_SME_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type_System__c</field>
            <operation>equals</operation>
            <value>SME Product</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Adhoc Charter,MICE,Agency Partnerships,Business and Government,GSA,SME Product,Sales Development,Charters,Future Planet,Global opportunity,Group Sales,B&amp;G Campaigns</value>
        </criteriaItems>
        <description>When a Lead (with opportunity Type selected as &apos;SME Product&apos;) is converted to opportunity this workflow will set the &apos;Type&apos; field in Opportunity to &apos;SME Product&apos;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>To update the type to Sales Development in Opportunity</fullName>
        <actions>
            <name>Set_type_to_Sales_Development</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type_System__c</field>
            <operation>equals</operation>
            <value>Sales Development</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Adhoc Charter,MICE,Agency Partnerships,Business and Government,GSA,SME Product,Sales Development,Charters,Future Planet,Global opportunity,Group Sales,B&amp;G Campaigns</value>
        </criteriaItems>
        <description>When a Lead (with opportunity Type selected as &apos;Sales Development&apos;) is converted to opportunity this workflow will set the &apos;Type&apos; field in Opportunity to &apos;Sales Development&apos;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>ClosureDue_date</fullName>
        <assignedToType>owner</assignedToType>
        <description>workflow task to notify the opportunity owner that the opportunity is due for closure (closure date – 11 months for the reminder).</description>
        <dueDateOffset>-335</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>ClosureDue date</subject>
    </tasks>
</Workflow>
