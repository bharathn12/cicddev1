<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Activity_update_Counter</fullName>
        <field>Activity_Update_Counter__c</field>
        <formula>IF( Activity_Update_Counter__c &gt;0, Activity_Update_Counter__c +1, 1)</formula>
        <name>Activity update Counter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Log_a_Call_checkbox_update</fullName>
        <description>To flag the checkbox for the records that are created using the 
log a call button. This is a system field required for reporting purposes</description>
        <field>Log_a_Call__c</field>
        <literalValue>1</literalValue>
        <name>Log a Call checkbox update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_subtype_for_completed_tasks</fullName>
        <field>Subtype__c</field>
        <literalValue>Action Taken</literalValue>
        <name>Update subtype for completed tasks</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Increment Activity update counter</fullName>
        <actions>
            <name>Activity_update_Counter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Task_type__c</field>
            <operation>equals</operation>
            <value>SME Callout</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>QIS-SME Callout</value>
        </criteriaItems>
        <description>Increment Activity update counter for SME Callout related calls</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Log a Call field in Task</fullName>
        <actions>
            <name>Log_a_Call_checkbox_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>( Created_Date_Time_System__c == Modified_Date_Time_System__c ) &amp;&amp; ISPICKVAL( Status , &quot;Completed&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update sub type for completed tasks</fullName>
        <actions>
            <name>Update_subtype_for_completed_tasks</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subtype__c</field>
            <operation>equals</operation>
            <value>Next Steps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>This is to update the sub type of the completed tasks.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
