<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Corporate_Scheme_Change_of_Coordinator_Details_notification_to_Loyalty</fullName>
        <ccEmails>BusGovSalesSolutions@qantas.com.au</ccEmails>
        <description>Corporate Scheme - Change of Coordinator Details notification to Loyalty</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>busgovsalessolutions@qantas.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Corporate_Scheme_Change_of_Coordinator</template>
    </alerts>
    <alerts>
        <fullName>Frequent_Flyer_Bulk_Request_Notification</fullName>
        <ccEmails>BusGovSalesSolutions@qantas.com.au</ccEmails>
        <description>Frequent Flyer - Bulk Request Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>jessicamarcellienus@qantas.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforcesupport@qantas.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Frequent_Flyer_Bulk_Request</template>
    </alerts>
    <alerts>
        <fullName>Frequent_Flyer_High_Priority_Notification</fullName>
        <ccEmails>BusGovSalesSolutions@qantas.com.au</ccEmails>
        <description>Frequent Flyer - High Priority Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>jessicamarcellienus@qantas.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforcesupport@qantas.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Frequent_Flyer_High_Priority</template>
    </alerts>
    <alerts>
        <fullName>Sales_Offers_Bulk_FF_Request_Notification</fullName>
        <ccEmails>BusGovSalesSolutions@qantas.com.au</ccEmails>
        <description>Sales Offers - Bulk FF Request Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>jessicamarcellienus@qantas.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforcesupport@qantas.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Sales_Offers_Bulk_Request</template>
    </alerts>
    <alerts>
        <fullName>Sales_Offers_Corporate_Trigger_50_Bonus_Trigger_Notification</fullName>
        <ccEmails>BusGovSalesSolutions@qantas.com.au</ccEmails>
        <description>Sales Offers - Corporate Trigger 50% Bonus Trigger Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>jessicamarcellienus@qantas.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforcesupport@qantas.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Sales_Offers_Corporate_Trigger_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Sales_Offer_Adhoc_Notification_to_SHR_B_G</fullName>
        <ccEmails>BusGovSalesSolutions@qantas.com.au</ccEmails>
        <description>Send Sales Offer Adhoc Notification to SHR B&amp;G</description>
        <protected>false</protected>
        <recipients>
            <recipient>fidelia.teoh@qantas.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>busgovsalessolutions@qantas.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Sales_Solution_Adhoc_Sales_Offers</template>
    </alerts>
    <alerts>
        <fullName>To_send_email_to_the_case_consultant</fullName>
        <description>To send email to the case consultant</description>
        <protected>false</protected>
        <recipients>
            <field>ConsultantEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Group_Waivers_case_creation</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Handover_Case_No_New_Team</fullName>
        <description>For Account Handover Case, flag No Team = TRUE</description>
        <field>No_New_Team__c</field>
        <literalValue>1</literalValue>
        <name>Account Handover Case - No New Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Review_Case_Closed_Status</fullName>
        <field>Status</field>
        <literalValue>Closed - Approved</literalValue>
        <name>Account Review Case Closed Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_Change</fullName>
        <field>Approval_Status__c</field>
        <name>Approval Status Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_asApproved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approval Status as Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Dealing_Model_to_QDM_Queue</fullName>
        <description>Assign dealing model requests with specific status to QDM Queue</description>
        <field>OwnerId</field>
        <lookupValue>QDM_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Dealing Model to QDM Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_QBR_cases_to_QBR_Data_Support</fullName>
        <field>OwnerId</field>
        <lookupValue>QBR_Data_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign QBR cases to QBR Data Support</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_owner_to_QDM_Report_Queue</fullName>
        <description>Assign ownership of Case to QDM Report Queue</description>
        <field>OwnerId</field>
        <lookupValue>QDM_Reports</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign owner to QDM Report Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_the_Case_to_the_Robot_Queue_for_D</fullName>
        <field>OwnerId</field>
        <lookupValue>Deal_Load_Robot_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign the Case to the Robot Queue for D</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Account_Review_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Account_Review_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Account Review Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Deal_Optimisation_Queue</fullName>
        <description>Assign dealing model requests with specific status to Deal Optimisation Queue.</description>
        <field>OwnerId</field>
        <lookupValue>Deal_Optimisation_Team_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Deal Optimisation Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Deal_Support_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Deal_Support_Request_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Deal Support Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_QDM_Queue</fullName>
        <description>Assign deal support cases with specific status to QDM Queue.</description>
        <field>OwnerId</field>
        <lookupValue>QDM_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to QDM Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_QIC_Sales_Support_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>QIC_Sales_Support_Escalation_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to QIC Sales Support Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Sales_Communication_Queue</fullName>
        <description>Assign the case to Sales Communication Queue</description>
        <field>OwnerId</field>
        <lookupValue>Sales_Communication_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Sales Communication Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Sales_Solution_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales_Solution_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Sales Solution Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_US_Sales_Support_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>US_Sales_Support_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to US Sales Support Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assigned_to_US_Sales_Support</fullName>
        <field>Status</field>
        <literalValue>Assigned to US Sales Support</literalValue>
        <name>Assigned to US Sales Support</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_Populate_Expected_Completion_10Day</fullName>
        <description>Auto Populate Expected Completion Date + 10days</description>
        <field>Expected_Completion_Date__c</field>
        <formula>IF(MOD(DATEVALUE(CreatedDate) - Date(1970,01,05) , 7) &lt;=3 , DATEVALUE(CreatedDate) + 10 + 2, 
IF(MOD(DATEVALUE(CreatedDate) - Date(1970,01,05) , 7) =4 , DATEVALUE(CreatedDate) + 10 + 4, 
IF(MOD(DATEVALUE(CreatedDate) - Date(1970,01,05) , 7) =5 , DATEVALUE(CreatedDate) + 10 + 3, 
DATEVALUE(CreatedDate) + 10 + 2)))</formula>
        <name>Auto Populate Expected Completion +10Day</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_Populate_Expection_Completion_Date</fullName>
        <field>Expected_Completion_Date__c</field>
        <formula>IF(MOD(DATEVALUE(CreatedDate) - Date(1970,01,05) , 7) &lt;=3 , DATEVALUE(CreatedDate) + 6 + 2, 
IF(MOD(DATEVALUE(CreatedDate) - Date(1970,01,05) , 7) =4 , DATEVALUE(CreatedDate) + 6 + 4, 
IF(MOD(DATEVALUE(CreatedDate) - Date(1970,01,05) , 7) =5 , DATEVALUE(CreatedDate) + 6 + 3, 
DATEVALUE(CreatedDate) + 6 + 2)))</formula>
        <name>Auto Populate Expection Completion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CP_Update_Default_Scheme_End_Date</fullName>
        <description>Update Default Scheme End Date from the Corporate Scheme End Date</description>
        <field>Default_Scheme_End_Date__c</field>
        <formula>Corporate_Scheme__r.Scheme_End_Date__c</formula>
        <name>CP Update Default Scheme End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CP_Update_Default_Scheme_Start_Date</fullName>
        <description>Update Default Scheme Start Date from the Corporate Scheme Start Date</description>
        <field>Default_Scheme_Start_Date__c</field>
        <formula>Corporate_Scheme__r.Scheme_Start_Date__c</formula>
        <name>CP Update Default Scheme Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CP_Update_Existing_Discount</fullName>
        <description>Update Existing %Discount from the Corporate Scheme Discount %</description>
        <field>Existing_Discount__c</field>
        <formula>Corporate_Scheme__r.Discount__c</formula>
        <name>CP Update Existing % Discount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Authority_Number_update_for_Blanke</fullName>
        <field>Authority_Number__c</field>
        <formula>Parent.Authority_Number__c</formula>
        <name>Case: Authority Number update for Blanke</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_Update_System_Admin</fullName>
        <field>OwnerId</field>
        <lookupValue>System_Admin_Request_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner Update - System Admin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Update_With_Salesforce_Spp</fullName>
        <description>Case Status = with Salesforce Support Queue</description>
        <field>Status</field>
        <literalValue>with Salesforce Support  Admin Team</literalValue>
        <name>Case Status Update - With Salesforce Spp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Owner_to_Sales_Performance</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales_Performance_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Update Owner to Sales Performance</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Owner_to_System_Admin_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>System_Admin_Request_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Update Owner to System Admin Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_owner_to_North_America_Distribution</fullName>
        <description>Update ownership the Deal Support Cases raised bu US users to North America Distribution queue</description>
        <field>OwnerId</field>
        <lookupValue>North_America_Distribution</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case owner to North America Distribution</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Case_to_D_C_Queue</fullName>
        <description>Change Case to D&amp;C Queue</description>
        <field>OwnerId</field>
        <lookupValue>D_C_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change Case to D&amp;C Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Owner_to_QDM_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>QDM_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change Owner to QDM Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Owner_to_System_Admin_Queue</fullName>
        <description>When a case with record type &quot;System Admin Request&quot; is created, the case is automatically assigned to the System Admin Request.</description>
        <field>OwnerId</field>
        <lookupValue>System_Admin_Request_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change Owner to System Admin Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Status_to_Assigned_for_Approval</fullName>
        <description>Change Status to Assigned for Approval</description>
        <field>Status</field>
        <literalValue>Assigned for Approval</literalValue>
        <name>Change Status to Assigned for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_the_Approved_checkbox</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Check the Approved checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateIschecked</fullName>
        <field>DateIschecked__c</field>
        <literalValue>1</literalValue>
        <name>DateIschecked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deal_Optimisation_Queue</fullName>
        <description>Update Case ownership to Dealing Optimisation Queue</description>
        <field>OwnerId</field>
        <lookupValue>Deal_Optimisation_Team_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Deal Optimisation Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_Coordinator_Phone</fullName>
        <description>Used to Update Corporate Scheme Coordinator Business Phone to Case Default Business Phone</description>
        <field>Default_Business_Phone__c</field>
        <formula>Corporate_Scheme__r.Business_Phone_Country_Code__c &amp;&quot; &quot;&amp; Corporate_Scheme__r.Business_Phone_Area_Code__c &amp;&quot; &quot;&amp;  Corporate_Scheme__r.Business_Phone_Line_Number__c</formula>
        <name>Default Coordinator Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Description</fullName>
        <field>Description</field>
        <formula>Justification__c</formula>
        <name>Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalate_to_field_update</fullName>
        <description>Sets the escalate field to blank when a case is reopened.</description>
        <field>Escalate_To__c</field>
        <name>Escalate to field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalate_to_field_update_to_blank</fullName>
        <description>Updates the escalate to field to blank when a case is closed.</description>
        <field>Escalate_To__c</field>
        <name>Escalate to field update to blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalated</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>Escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalation_Assignment_to_US_Queue</fullName>
        <description>Assigning the escalated case to the US Sales Support Queue</description>
        <field>OwnerId</field>
        <lookupValue>US_Sales_Support_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Escalation Assignment to US Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalation_Flag_Ticked</fullName>
        <description>The Escalated flag is ticked when a service request is escalated to the US Sales Support Queue</description>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>Escalation Flag Ticked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Group_Waivers_Team_Alpha</fullName>
        <field>OwnerId</field>
        <lookupValue>Group_Sales_Waivers_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Group Waivers - Team Alpha</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>In_Approval_Process</fullName>
        <field>Status</field>
        <literalValue>With Account Owner&apos;s Manager</literalValue>
        <name>In Approval Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_Approved_By_update</fullName>
        <description>To update the field &apos;Approval Received From&apos;</description>
        <field>Most_Recent_Approver__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>Last Approved By update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAM_Approved_is_Ticked</fullName>
        <description>NAM Approved is Ticked</description>
        <field>NAM_s_Approval__c</field>
        <literalValue>1</literalValue>
        <name>NAM Approved is Ticked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_with_Sales_Solutions</fullName>
        <description>Case Status = Pending with Sales Solutions</description>
        <field>Status</field>
        <literalValue>Pending with Sales Solutions</literalValue>
        <name>Pending with Sales Solutions</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QIC_Escalation_Assignment_to_Level6</fullName>
        <field>OwnerId</field>
        <lookupValue>QIC_Level_6_Escalation_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>QIC Escalation Assignment to Level6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QIC_Escalation_Flag</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>QIC Escalation Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QIC_escalation</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>QIC escalation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QIC_escalation_assignment_Sales_support</fullName>
        <field>OwnerId</field>
        <lookupValue>QIC_Sales_Support_Escalation_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>QIC escalation assignment-Sales support</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recent_Approval_Date_Time</fullName>
        <description>To capture the date/time of the most recent approval</description>
        <field>Approval_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Recent Approval Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject_process</fullName>
        <field>Type</field>
        <literalValue>Deal Termination</literalValue>
        <name>Reject process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_Approval_Date_Time</fullName>
        <field>Approval_Date_Time__c</field>
        <name>Remove Approval Date/ Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_RecordType_to_QIC</fullName>
        <description>tradesite cases should have QIC recordTypes</description>
        <field>RecordTypeId</field>
        <lookupValue>QIC_Service_request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Case RecordType to QIC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_asApproved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Status as Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_as_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved &amp; Assigned to Loyalty</literalValue>
        <name>Status as Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_as_Pending</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Status as Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_as_Reject</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Reject</literalValue>
        <name>Status as Reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_update</fullName>
        <field>Status</field>
        <literalValue>Assigned-Deal Support-iDeal loading</literalValue>
        <name>Status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_with_Deal_Optimisation_Queue</fullName>
        <field>Status</field>
        <literalValue>with Deal Optimisation Team</literalValue>
        <name>Status with Deal Optimisation Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Subject</fullName>
        <field>Subject</field>
        <formula>&quot;Group Sales:&quot; +&quot; &quot;+ Consultants_Name__c +&quot; &quot;+ IATA__c +&quot; &quot;+ TEXT(Team_Lead__c)</formula>
        <name>Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_the_Approved_CheckBox</fullName>
        <field>Approved__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck the Approved CheckBox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Upd_Status_to_Not_yet_assigned_with_AM</fullName>
        <field>Status</field>
        <literalValue>Not yet assigned - with Account Manager</literalValue>
        <name>Upd Status to Not yet assigned - with AM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Owner</fullName>
        <description>Update Previous Account Owner with Account Owner</description>
        <field>Previous_Owner__c</field>
        <formula>Account.Owner.FirstName &amp; &quot; &quot; &amp; Account.Owner.LastName</formula>
        <name>Update Account Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Review_Close_Date</fullName>
        <field>Auto_Account_Review_Case_Date__c</field>
        <formula>CASE( 
  MOD( DATEVALUE(LastModifiedDate) - DATE( 1900, 1, 7 ), 7 ),
  1, DATEVALUE(LastModifiedDate) + 2 + 5,
  2, DATEVALUE(LastModifiedDate) + 2 + 5,
  3, DATEVALUE(LastModifiedDate) + 2 + 5,
  4, DATEVALUE(LastModifiedDate) + 2 + 5,
  5, DATEVALUE(LastModifiedDate) + 2 + 5,
  6, DATEVALUE(LastModifiedDate) + 1 + 5,
  DATEVALUE(LastModifiedDate) + 5
)</formula>
        <name>Update Account Review Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_as_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Update Approval Status as Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Blanket_Waiver_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Blanket_waiver_Closed_Cases</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Blanket Waiver Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Owner_to_SF_Support_Team</fullName>
        <description>Update Case Owner to Salesforce Support Team</description>
        <field>OwnerId</field>
        <lookupValue>System_Admin_Request_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Case Owner to SF Support Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Owner_to_SP_Team</fullName>
        <description>Update Case Owner to Sales Performance Team</description>
        <field>OwnerId</field>
        <lookupValue>Sales_Performance_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Case Owner to SP Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Default_Coordinator_Email</fullName>
        <description>Used to Update Corporate Scheme Coordinator Email to Case Default Email</description>
        <field>Default_Coordinator_Email__c</field>
        <formula>Corporate_Scheme__r.Key_Coordinator_Email__c</formula>
        <name>Update Default Coordinator Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Default_Coordinator_Name</fullName>
        <description>Used to Update Corporate Scheme Coordinator Name to Case Default Key Coordinator</description>
        <field>Default_Coordinator_Name__c</field>
        <formula>Corporate_Scheme__r.Key_Coordinator__c</formula>
        <name>Update Default Coordinator Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Default_Coordinator_Preference</fullName>
        <description>Used to Update Corporate Scheme Type to Case Default Type</description>
        <field>Default_Type_of_Coordinator__c</field>
        <formula>TEXT ( Corporate_Scheme__r.Type__c )</formula>
        <name>Update Default Coordinator Preference</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Portfolio</fullName>
        <description>For Account Handover, update Previous Portfolio with Account Owner Reporting Team</description>
        <field>Previous_Portfolio__c</field>
        <formula>Account.Owner.Reporting_Team__c</formula>
        <name>Update Previous Portfolio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Step_1_Approved</fullName>
        <field>Status</field>
        <literalValue>With NAM</literalValue>
        <name>Update Status Step 1 - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_as_Waiting_for_Approval</fullName>
        <field>Status</field>
        <literalValue>Pending Approval</literalValue>
        <name>Update Status as Waiting for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved_Case</fullName>
        <description>Update Status to Approved</description>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Update Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_New</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Update Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Waiting_for_Team_Lead_t</fullName>
        <field>Status</field>
        <literalValue>Waiting for Team Lead to update the Account Name</literalValue>
        <name>Update Status to Waiting for Team Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_with_Head_of_QIS</fullName>
        <field>Status</field>
        <literalValue>with Head of QIS</literalValue>
        <name>Update Status - with Head of QIS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_with_QDM_Team</fullName>
        <description>Update Case Status to &quot;with QDM Team&quot;</description>
        <field>Status</field>
        <literalValue>with QDM Team</literalValue>
        <name>Update Status - with QDM Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_with_Team_Lead</fullName>
        <field>Status</field>
        <literalValue>with Team Lead</literalValue>
        <name>Update Status - with Team Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_escalated_checkbox_to_blank</fullName>
        <description>Updates the escalated check box to blank, when a case is closed rejected</description>
        <field>IsEscalated</field>
        <literalValue>0</literalValue>
        <name>Update escalated checkbox to blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_for_the_approval</fullName>
        <field>NAM_s_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Update for the approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_layout_to_QIC_closed_rejected</fullName>
        <field>RecordTypeId</field>
        <lookupValue>QIC_Closed_Rejected_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update layout to QIC closed rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_reopened_date_and_time</fullName>
        <field>Reopened_Date__c</field>
        <formula>NOW()</formula>
        <name>Update reopened date and time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved &amp; Assigned to Loyalty</literalValue>
        <name>Update status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Update status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_Waiting_for_Approval</fullName>
        <field>Status</field>
        <literalValue>Waiting for Approval</literalValue>
        <name>Update status to Waiting for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Consultant_Email</fullName>
        <field>ConsultantEmail__c</field>
        <formula>TEXT(Consultant__c)</formula>
        <name>Update the Consultant Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_TYPE_field_to_Frequent_Flyer</fullName>
        <description>To update the TYPE field to &quot;Frequent Flyer Information&quot;</description>
        <field>Type</field>
        <literalValue>Frequent Flyer Information</literalValue>
        <name>Update the TYPE field to Frequent Flyer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>assign_to_DRA_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>DRA_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>assign to DRA queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account Handover - No Team added</fullName>
        <actions>
            <name>Account_Handover_Case_No_New_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.New_Team__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Not yet assigned - with Account Manager</value>
        </criteriaItems>
        <description>If there is no Team chosen in the Account Handover case, flag No Team checkbox to TRUE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Handover - Other Type Handover</fullName>
        <actions>
            <name>Case_Owner_Update_System_Admin</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Status_Update_With_Salesforce_Spp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 3 AND (2 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Handover</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.New_Portfolio__c</field>
            <operation>equals</operation>
            <value>Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>with Sales Performance Team,with Salesforce Support  Admin Team,With Head of QIS &amp; National Manager,with Head of QIS,Closed - Account moved,Closed - Incomplete Details,With Account Owner&apos;s Manager,With NAM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Other</value>
        </criteriaItems>
        <description>When Type = Other, workflow will automatically change Status = Salesforce Support and Case Owner = System Admin Queue Request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Handover - Previous Ownership</fullName>
        <actions>
            <name>Update_Account_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_Portfolio</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Handover</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Not yet assigned - with Account Manager</value>
        </criteriaItems>
        <description>For Account Handover, previous Account Owner and Reporting Team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Au service requests to QIC Sales Support Queue</fullName>
        <actions>
            <name>Assign_to_QIC_Sales_Support_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>AU</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Service Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>In the Approval process - &quot;Manual Approval for QIC Case&quot; when the record is approved this workflow will be triggered.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign QBR Cases</fullName>
        <actions>
            <name>Assign_QBR_cases_to_QBR_Data_Support</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>QBR Data Support</value>
        </criteriaItems>
        <description>to handle QBR Cases created by ETL</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign cases to the account review queue</fullName>
        <actions>
            <name>Assign_to_Account_Review_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Review Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Assigned to Deal Support</value>
        </criteriaItems>
        <description>For the Account Review cases record type, the cases are to be assigned to the &apos;Account Review Queue&apos;, when the status is &apos;Assigned to Deal Support&apos;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign deal support requests to QDM queue</fullName>
        <actions>
            <name>Assign_to_QDM_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>With QDM - iDeal approval,Complete - Pending PRISM Deal Load</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.CountryCode</field>
            <operation>notEqual</operation>
            <value>US</value>
        </criteriaItems>
        <description>To assign deal support cases with specific status to QDM Queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign deal support requests to QDM queue v1</fullName>
        <actions>
            <name>Assign_to_QDM_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Complete - Pending PRISM Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.CountryCode</field>
            <operation>notEqual</operation>
            <value>US</value>
        </criteriaItems>
        <description>To assign deal support cases with specific status to QDM Queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign dealing model requests to Deal Optimisation queue</fullName>
        <actions>
            <name>Assign_to_Deal_Optimisation_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>with Deal Optimisation for processing,with Deal Optimisation - INT Model</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Dealing Model Request</value>
        </criteriaItems>
        <description>To assign dealing model requests with specific status to Deal Optimisation Queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign dealing model requests to QDM queue</fullName>
        <actions>
            <name>Assign_Dealing_Model_to_QDM_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>with QDM Team</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Dealing Model Request</value>
        </criteriaItems>
        <description>To assign dealing model requests with specific status to QDM Queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign the Case to the Robot Queue for Deal Loading</fullName>
        <actions>
            <name>Assign_the_Case_to_the_Robot_Queue_for_D</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Assign_it_for_the_Deal_Loading__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign to D%26C Queue</fullName>
        <actions>
            <name>Change_Case_to_D_C_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Assign to D&amp;C queue when &quot;Build Comms&quot; is selected as the Status as part of Sales Communication Parent Case</description>
        <formula>OR(AND( ISPICKVAL(Status, &apos;Build Comms&apos;),  RecordType.Id = &apos;01290000001MHNZ&apos;,  ISCHANGED(Status)), (ISPICKVAL(Status, &apos;With D&amp;C&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to Deal Support Queue</fullName>
        <actions>
            <name>Assign_to_Deal_Support_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Assigned-Deal Support-iDeal loading,Assigned to Pricing for Fare Filing</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.CountryCode</field>
            <operation>notEqual</operation>
            <value>US</value>
        </criteriaItems>
        <description>To assign deal support cases with specific status to Deal Support Queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign to Sales Communication Queue</fullName>
        <actions>
            <name>Assign_to_Sales_Communication_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Assign the Sales Communication Parent Case to Sales Communication Queue when any of the following statuses: &quot;Revision&quot;, &quot;Corporate Comms Review&quot;, &quot;Final Review&quot;, &quot;Build Approval&quot; is selected</description>
        <formula>AND( OR(ISPICKVAL(Status, &apos;Revision&apos;),  ISPICKVAL(Status, &apos;Corporate Comms Review&apos;),  ISPICKVAL(Status, &apos;Final Review&apos;),  ISPICKVAL(Status, &apos;Build Approval&apos;)),  RecordType.Id = &apos;01290000001MHNZ&apos;,  ISCHANGED(Status))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to US North America Dist Queue</fullName>
        <actions>
            <name>Case_owner_to_North_America_Distribution</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Assigned-Deal Support-iDeal loading</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.CountryCode</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>To assign Deal support cases with specific status to US North America Distribution Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assignment of Cases to the System Admin Queue</fullName>
        <actions>
            <name>Change_Owner_to_System_Admin_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The workflow assigns the case to the System Admin queue when a case with record type &quot;SystemAdmin Request&quot; is submitted.</description>
        <formula>AND (RecordType.Name = &quot;System Admin Request&quot;, $Profile.Name &lt;&gt; &quot;System Administrator&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assignment of DRA Cases to the DRA Queue</fullName>
        <actions>
            <name>assign_to_DRA_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>DRA Request</value>
        </criteriaItems>
        <description>Assignment of DRA Cases to the DRA Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assignment of QDM Report Cases to the QDM Report Queue</fullName>
        <actions>
            <name>Assign_owner_to_QDM_Report_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>QDM Report Request</value>
        </criteriaItems>
        <description>Assignment of QDM Report Cases to the QDM Report Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Auto Close Account Review Case after 5 business Days</fullName>
        <actions>
            <name>Update_Account_Review_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Review Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Account_Review_Case_Closed_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Auto_Account_Review_Case_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Auto Populate Expected Completion Date</fullName>
        <actions>
            <name>Auto_Populate_Expection_Completion_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>DateIschecked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Dealing Model Request,General Dealing Enquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Expected_Completion_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Update Owner to Sales Performance</fullName>
        <actions>
            <name>Case_Update_Owner_to_Sales_Performance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>with Sales Performance Team</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Update Owner to System Admin Queue</fullName>
        <actions>
            <name>Case_Update_Owner_to_System_Admin_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>with Salesforce Support  Admin Team</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Handover</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>B&amp;G to QIS,QIS to B&amp;G,B&amp;G to B&amp;G,QIS to QIS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Blanket Waiver Auth Number</fullName>
        <actions>
            <name>Case_Authority_Number_update_for_Blanke</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISPICKVAL( Status ,&apos;Closed - Approved&apos;)&amp;&amp; ISPICKVAL(Type,&apos;Operational Waiver&apos;) &amp;&amp;  ( RecordType.Name  == &apos;Service Request&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Corporate Scheme - Change of Coordinator</fullName>
        <actions>
            <name>Default_Coordinator_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Default_Coordinator_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Default_Coordinator_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Default_Coordinator_Preference</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Type = Change of Coordinator OR Change of Coordinator details? = TRUE, update Default Coordinator Details</description>
        <formula>AND ( RecordTypeId = &quot;01290000001Ybqs&quot;, /*Corporate Scheme Service Request*/   OR (  ISPICKVAL ( Type , &quot;Change of Coordinator details&quot;),  Change_of_Coordinator_Details__c = TRUE) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Corporate Scheme - Incorrect Case</fullName>
        <actions>
            <name>Change_Owner_to_System_Admin_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Actioned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Incorrect Account Linked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Corporate Scheme Service Request</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Corporate Scheme - Merge Case Request</fullName>
        <actions>
            <name>Assign_to_Sales_Solution_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Actioned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>notEqual</operation>
            <value>Incorrect Account Linked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Corporate Scheme Service Request</value>
        </criteriaItems>
        <description>When Corporate Scheme Service Request = Merge Corporate Scheme, send to Sales Solution Team Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Corporate Scheme - Renewal Case</fullName>
        <actions>
            <name>CP_Update_Default_Scheme_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CP_Update_Default_Scheme_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CP_Update_Existing_Discount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Corporate Scheme Service Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Renewal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Auto_Create_Case__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Upon manual creation of Corporate Scheme Renewal Case, update Default Scheme Start and End Date, and Existing %Discount</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Corporate Scheme - Send notification to Loyalty</fullName>
        <actions>
            <name>Corporate_Scheme_Change_of_Coordinator_Details_notification_to_Loyalty</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sent_Change_of_Coordinator_Notifcation</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND (  RecordTypeId = &quot;01290000001Ybqs&quot;, /*Corporate Scheme Service Request*/   Send_Change_of_Coordinator_email__c = TRUE, OR (  ISPICKVAL ( Type , &quot;Change of Coordinator details&quot;),  Change_of_Coordinator_Details__c = TRUE)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Record Type for Tradesite Cases to QIC</fullName>
        <actions>
            <name>Set_Case_RecordType_to_QIC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.TradeSite_Request_Id__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>tradesite cases should be assigned to QIC record types</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FF Request - Pending with Sales Solutions Queue</fullName>
        <actions>
            <name>Assign_to_Sales_Solution_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending with Sales Solutions</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Frequent Flyer Request</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Frequent Flyer - Bulk Request Notification</fullName>
        <actions>
            <name>Frequent_Flyer_Bulk_Request_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Frequent Flyer Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Bulk_Frequent_Flyer_Request__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>For Frequent Flyer Cases, Bulk Request = TRUE, email notification will be sent to SHR BUSGOV</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Frequent Flyer - High Priority Notification</fullName>
        <actions>
            <name>Frequent_Flyer_High_Priority_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Frequent Flyer Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>For Frequent Flyer Cases, if Priority = High, email notification will be sent to SHR BUSGOV</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Group Waivers - Case assignment to Queue</fullName>
        <actions>
            <name>Group_Waivers_Team_Alpha</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Group Waivers</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Group Waivers - Escalated</fullName>
        <actions>
            <name>Escalated</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Within_BART__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Group Waivers</value>
        </criteriaItems>
        <description>If the Group Waivers form is having &apos;Within BART&apos; is &apos;NO&apos;, then &apos;Escalated&apos; is to be checked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Group Waivers - Non Escalated</fullName>
        <actions>
            <name>Update_Status_to_Waiting_for_Team_Lead_t</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Within_BART__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Group Waivers</value>
        </criteriaItems>
        <description>If the Group Waivers form is having &apos;Within BART&apos; is &apos;No&apos;, then update the case status to Waiting for Team Lead to update the Account Name</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Group Waivers - Subject and Description Update</fullName>
        <actions>
            <name>Description</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Group Waivers</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Group Waivers - case creation notification</fullName>
        <actions>
            <name>To_send_email_to_the_case_consultant</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_the_Consultant_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Group Waivers</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Consultant__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Handover Case - Update Owner Sales Performance Team</fullName>
        <actions>
            <name>Update_Case_Owner_to_SP_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>with Sales Performance Team</value>
        </criteriaItems>
        <description>When Status = with Sales Performance Team</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NAM Approved Gets Ticked when NAM Raise a Deal Ext or Termination Case</fullName>
        <actions>
            <name>NAM_Approved_is_Ticked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Manager_s_Role_ID__c</field>
            <operation>equals</operation>
            <value>00E90000001P6J4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Deal Extension,Deal Termination</value>
        </criteriaItems>
        <description>This is to cater for the deal extension/termination process for NAMs as they do not need to go through an approval process. Hence, the NAM approved checkbox gets ticked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pending with Sales Solutions</fullName>
        <actions>
            <name>Pending_with_Sales_Solutions</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending with Sales Solutions</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>QIC Escalation to Level6</fullName>
        <actions>
            <name>QIC_Escalation_Assignment_to_Level6</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QIC_Escalation_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Escalate_To__c</field>
            <operation>equals</operation>
            <value>QIC</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>QIC Escalation to Sales Support</fullName>
        <actions>
            <name>QIC_Escalation_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QIC_escalation_assignment_Sales_support</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Escalate_To__c</field>
            <operation>equals</operation>
            <value>Sales Support</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sales Offer - Bulk Request Notification</fullName>
        <actions>
            <name>Sales_Offers_Bulk_FF_Request_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Offers Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Bulk_Frequent_Flyer_Request__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>For Sales Offers Cases, Bulk Request = TRUE, email notification will be sent to SHR BUSGOV</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sales Offers - Corporate Trigger Notification</fullName>
        <actions>
            <name>Sales_Offers_Corporate_Trigger_50_Bonus_Trigger_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Offers Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Type__c</field>
            <operation>equals</operation>
            <value>Corporate Trigger - 50% Bonus Status</value>
        </criteriaItems>
        <description>For Sales Offers, Sub Type = Corporate Trigger - 50% Bonus Status, email notification is sent to SHR BUSGOV.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sys Admin - Update Expected Completion Date</fullName>
        <actions>
            <name>Auto_Populate_Expected_Completion_10Day</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>System Admin Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Expected_Completion_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>For System Admin Cases, update Expected Completion Date + 10 Days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>US Waivers Escalation</fullName>
        <actions>
            <name>Escalation_Assignment_to_US_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Escalation_Flag_Ticked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Escalate_To__c</field>
            <operation>equals</operation>
            <value>US Sales Support Queue</value>
        </criteriaItems>
        <description>When &quot;US Sales Support Queue&quot; is selected in the &quot;Escalate To&quot; drop-down, the Escalated checkbox should be ticked and the case should be assigned to the US Sales Support Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Authority Number</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Authority_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Ownership to DO Queue</fullName>
        <actions>
            <name>Deal_Optimisation_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Status_with_Deal_Optimisation_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>General Dealing Enquiry</value>
        </criteriaItems>
        <description>When Status = with Deal Optimisation Team</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update QIC request reopen date time</fullName>
        <actions>
            <name>Update_reopened_date_and_time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Reopened</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update the Type value</fullName>
        <actions>
            <name>Update_the_TYPE_field_to_Frequent_Flyer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Frequent Flyer Request</value>
        </criteriaItems>
        <description>When a Frequent Flyer Request is created, the TYPE should be set as Frequent Flyer Information</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update to Blanket Waiver closed Status layout</fullName>
        <actions>
            <name>Update_Blanket_Waiver_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Rejected,Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Request,Master Blanket Waiver</value>
        </criteriaItems>
        <description>This workflow is used to auto update Blanket Waiver closed Status requests page layout where user is allowed to reopen the Balnket waiver closed request.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update to QIC closed rejected layout</fullName>
        <actions>
            <name>Escalate_to_field_update_to_blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_escalated_checkbox_to_blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_layout_to_QIC_closed_rejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Request</value>
        </criteriaItems>
        <description>This workflow is used to auto update QIC closed rejected requests to closed rejected  page layout where user is allowed to reopen the QIC closed request.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Sent_Change_of_Coordinator_Notifcation</fullName>
        <assignedToType>owner</assignedToType>
        <description>You have send the Change of Coordinator Details to qcschemerenewals@qantasloyalty.com</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Sent Change of Coordinator Notifcation</subject>
    </tasks>
</Workflow>
