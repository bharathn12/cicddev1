<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_expected_reporting_date</fullName>
        <field>Expected_Reporting_Date__c</field>
        <formula>DATE( 
year(EndDate) 
+ floor((month(EndDate) + 2)/12) + if(and(month(EndDate)=12,2&gt;=12),-1,0) 
, 
if( mod( month(EndDate) + 2, 12 ) = 0, 12 , mod( month(EndDate) + 2, 12 )) 
, 
min( 
day(EndDate), 
case( 
max( mod( month(EndDate) + 2, 12 ) , 1), 
9,30, 
4,30, 
6,30, 
11,30, 
2,28, 
31 
) 
) 
)</formula>
        <name>Update expected reporting date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_the_standard_Actual_field</fullName>
        <description>to update the standard actual field sourcing value from the custom Actual field.</description>
        <field>ActualCost</field>
        <formula>Child_Actual_Cost__c</formula>
        <name>update the standard Actual field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update expected reporting date</fullName>
        <actions>
            <name>Update_expected_reporting_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.EndDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Expected_Reporting_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>To set expected reporting date to 2 months from campaign end date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update the Actual</fullName>
        <actions>
            <name>update_the_standard_Actual_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Marketing - Child</value>
        </criteriaItems>
        <description>To update the standard Actual field.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
