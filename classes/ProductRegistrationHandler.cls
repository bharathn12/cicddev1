//**********************************************************************************************************************************
 
    /*Created Date: 18/03/2015
 
    Description: Process Product Registration trigger for:
                 1. processing AMEX Eligibility on accounts.
                 2. Opportunity creation based on travel frequency. (Disabling the creation through the JIRA CRM-1902) 
  
    Version:
    V1.0 - 18/03/2015 - Initial version [FO]*/
    //Membership_Expiry_Date__c replaces Aquire_membership_end_date__c CRM-1093  13/06/2015
    //Removed QBD Account references 
    //CRM-1573 (PR2810494) Recurring Amex requests need logic to run again.
    //CRM-1902 - Remove auto-create opp for Aquire
//**********************************************************************************************************************************
public class ProductRegistrationHandler {
   
     /*
      Purpose:  Create opportunity based on travel frequency, Change account type
      Parameters: trigger.new list of product Registrations.
      Return: none. 
    */
    public static boolean hasRunProcessAMEXEligibilityUpdate = false;
    public void processAccountAndOpportunity(List<Product_Registration__c> newList){
        String aquireRecordTypeId = Schema.SObjectType.Product_Registration__c.getRecordTypeInfosByName().get('Aquire Registration').getRecordTypeId();
        String amexRecordTypeId = Schema.SObjectType.Product_Registration__c.getRecordTypeInfosByName().get('AEQCC Registration').getRecordTypeId();
        
        List<Id> aquireAccounts = new List<Id>();
        List<Id> amexAccounts = new List<Id>();
                
        List<account> accToUpdate = new List<account>();
        
        for(Product_Registration__c pr:newList){
            if(pr.RecordTypeId == aquireRecordTypeId){
                aquireAccounts.add(pr.Account__c);
            }else if(pr.RecordTypeId == amexRecordTypeId){
                amexAccounts.add(pr.Account__c);
            }
        }
        
        /*Create oppertunities */
        
        //CRM-1902 - Commented the below lines to disable the auto Opportunity creation
        
        /*List<Opportunity> oppoList = new List<Opportunity>();
        Account_Travel_Frequency__c atf = Account_Travel_Frequency__c.getValues('Travel Frequency');*/
        
        for(Product_Registration__c pr:[SELECT id, Account__c, Annual_Domestic_Flights__c, Annual_International_Flights__c, Account__r.Name, Account__r.OwnerId,
                                               Account__r.CreatedDate, Account__r.Manual_Revenue_Update__c, Stage_Status__c, Estimated_Total_Annual_Revenue__c, Aq_Business_Domestic_annual_spend__c, Aq_Business_international_annual_spend__c 
                                        FROM Product_Registration__c 
                                        WHERE id IN:(Trigger.newMap.keySet()) AND Account__c IN:(aquireAccounts)]){
                                            
                //CRM-1902 - Commented the below lines to disable the auto Opportunity creation
            /*if(pr.Stage_Status__c == 'Active' && (isCreateOpportunity(pr.Annual_Domestic_Flights__c, atf.Travel_Frequency__c ) 
                                                  || isCreateOpportunity(pr.Annual_International_Flights__c, atf.Travel_Frequency__c ))){
              if(Trigger.isInsert){  oppoList.add(new Opportunity(Name = pr.Account__r.Name+' Aquire '+DateTime.now().format('dd-MM-YYYY'), 
                                             OwnerId   = pr.Account__r.OwnerId,
                                             AccountId = pr.Account__c, 
                                             StageName = 'Identify',
                                             CloseDate = pr.Account__r.CreatedDate.Date().addYears(1),
                                             Type      = 'Business and Government',
                                             Sub_Type_Level_1__c = 'Not Currently In Contract',
                                             Sub_Type_Level_2__c = 'New Contract',
                                             Sub_Type_Level_3__c = 'Single POS',
                                             Category__c = 'Qantas Business Savings',
                                             Amount = 0                                             
                                             )); 
               }
            } */
            
             if(pr.Account__r.Manual_Revenue_Update__c != true){
                if(pr.Aq_Business_Domestic_annual_spend__c == null && pr.Aq_Business_international_annual_spend__c == null){
                    accToUpdate.add(New Account(id = pr.Account__c, Aquire__c = true, Estimated_Total_Air_Travel_Spend__c  = null));
                }else {
                    Decimal a=0, b=0;
                    if(pr.Aq_Business_Domestic_annual_spend__c != null && pr.Aq_Business_Domestic_annual_spend__c != '') a =  Decimal.valueOf(rectifySpendAmount(pr.Aq_Business_Domestic_annual_spend__c));
                    if(pr.Aq_Business_international_annual_spend__c != null && pr.Aq_Business_international_annual_spend__c != '') b =  Decimal.valueOf(rectifySpendAmount(pr.Aq_Business_international_annual_spend__c));
                    Decimal amnt = a+b ;
                    accToUpdate.add(New Account(id = pr.Account__c, Aquire__c = true, Estimated_Total_Air_Travel_Spend__c  = amnt ));
                }
                
            }else{
                accToUpdate.add(New Account(id = pr.Account__c, Aquire__c = true));
            }            
        }
        
        //CRM-1902 - Commented the below lines to disable the auto Opportunity creation
        
        /* Save Opportunities */
       /* if(oppoList.size() > 0){
           if(oppoList.size()>0) Database.insert(oppoList);
        }*/
        
        /* Update Account types */
        for(id aId : amexAccounts){
            accToUpdate.add(New Account(id = aId, AMEX__c = true));
        }
      
        if(accToUpdate.size() > 0){
            try{
                Database.update(accToUpdate);
            } catch(Exception e){
                system.debug('EXCEPTION!!'+e.getStackTraceString());
            } 
        }
        
        
    }
    
    //CRM-1902 - Commented the below lines to disable the auto Opportunity creation
    
    /*
      Purpose:  Check for opportunity creation.
      Parameters: Account picklist value, Custom config value
      Return: boolean. 
    */
    /*private boolean isCreateOpportunity(String pickValue, String configValue){
        if(String.isBlank(pickValue) || String.isBlank(configValue)){
            return false;
        }
        if(pickValue == configValue){
             return true;
        }
        if(!configValue.isNumeric()){
             return false;
        }
        pickValue = pickValue.trim();
        pickValue = pickValue.replace('+', '');
        for(String strNum : pickValue.split('-', 0)){
            if(String.isNotBlank(strNum) && strNum.isNumeric() && (Integer.valueOf(strNum) >= Integer.valueOf(configValue))){
                return true;
            }
        }
        return false;
    }*/

    /*
      Purpose:  Process AMEX eligibility on insert trigger
      Parameters: trigger.new list of product Registrations.
      Return: none. 
    */
    public void processAMEXEligibility(List<Product_Registration__c> newList){
        
        List<Product_Registration__c> updateProducts = new List<Product_Registration__c>();  
        list<Account> updateAccounts = new list<Account>();
        
        Map<Id, Account> mapOfAccounts = this.getAccounts(newList);
        Map<Id, Product_Registration__c> aquireProducts = getAquireProductRegistrations(mapOfAccounts.keySet());
                
        for(Product_Registration__c pr : this.getProductRegistrationRecords('AEQCC Registration', newList)){
            Account acc = mapOfAccounts.get(pr.Account__c);              
          /*
           ** Commented for CRM-326 as discussed with Ivan  
                                                       
            if(Date.today() == acc.CreatedDate.date()){
                
                //pr.Agreed_change_date_by_Qantas__c = Date.today();
                                            
                if(pr.Customer_s_Requested_Choice__c == 'A'){
                    acc.Qantas_Accept_or_Reject_code__c = '1';
                    acc.AMEX_Response_Code__c = 'R';
                } else if(pr.Customer_s_Requested_Choice__c == 'R'){
                    acc.Qantas_Accept_or_Reject_code__c = '0';
                    acc.AMEX_Response_Code__c = 'R';                    
                }             
            
            }else if(Date.today() != acc.CreatedDate.date()){*/
                
                //line below is uncommented to fix CRM-1079
                //pr.Agreed_change_date_by_Qantas__c = Date.today(); **commented as part of CRM-1080
                pr.Agreed_change_date_by_Qantas__c = pr.AMEX_Customer_Choice_Date__c;
                
                if(acc.Agency__c == 'N' && acc.Dealing_Flag__c == 'N' && acc.Aquire_Override__c == 'N'){
                     
                     if(pr.Customer_s_Requested_Choice__c == 'A'){
                        
                        if(aquireProducts.containsKey(pr.account__c) && aquireProducts.get(pr.account__c).Stage_Status__c == 'Active'){
                            acc.Qantas_Accept_or_Reject_code__c = '0';
                            acc.AMEX_Response_Code__c = 'D';
                        //}else if(acc.Aquire__c == false){
                        }else{
                            acc.Qantas_Accept_or_Reject_code__c = '1';
                            acc.AMEX_Response_Code__c = 'R';
                        }
                        
                     } else if(pr.Customer_s_Requested_Choice__c == 'R'){
                       
                        acc.Qantas_Accept_or_Reject_code__c = '0';
                        acc.AMEX_Response_Code__c = 'R';                    
                     
                     }       
                }else if(acc.Agency__c == 'Y'){
                    acc.Qantas_Accept_or_Reject_code__c = '2';
                    acc.AMEX_Response_Code__c = 'I';
                }else if(acc.Dealing_Flag__c == 'Y'){
                    acc.Qantas_Accept_or_Reject_code__c = '3';
                    acc.AMEX_Response_Code__c = 'I';
                }else if(acc.Aquire_Override__c == 'Y'){
                    /*acc.Qantas_Accept_or_Reject_code__c = '4';
                    **replaced with condition below as part of CRM-1026
                    */
                    if(pr.Customer_s_Requested_Choice__c == 'A'){
                        acc.Qantas_Accept_or_Reject_code__c = '4';
                    }else if(pr.Customer_s_Requested_Choice__c == 'R'){
                        acc.Qantas_Accept_or_Reject_code__c = '0';
                    }
                    acc.AMEX_Response_Code__c = 'R';
                }
            /* } */
            updateAccounts.add(acc);
        }
        
        if(updateAccounts.size()>0){  
          try{
            dataBase.update(updateAccounts);
          }catch(Exception e){
             system.debug('EXCEPTION!!'+e.getStackTraceString());
          }  
       }        
    }
    
    /*
      Purpose:  Process AMEX eligibility on update trigger
      Parameters: trigger.new list of product Registrations, trigger.old list of product Registrations
      Return: none. 
    */
    public void processAMEXEligibility(List<Product_Registration__c> newList, Map<Id, Product_Registration__c> oldMap){
        if(!hasRunProcessAMEXEligibilityUpdate){
        List<Product_Registration__c> updateProducts = new List<Product_Registration__c>();  
        list<Account> updateAccounts = new list<Account>();
                
        Map<Id, Account> mapOfAccounts = this.getAccounts(newList);        
        Map<Id, Product_Registration__c> aquireProducts = getAquireProductRegistrations(mapOfAccounts.keySet());
        string amexRecTypeId = Schema.SObjectType.Product_Registration__c.getRecordTypeInfosByName().get('AEQCC Registration').getRecordTypeId();
        for(Product_Registration__c pr : newList){
            system.debug ('before:'+pr.Agreed_change_date_by_Qantas__c);
            if(pr.RecordTypeId == amexRecTypeId){
              Account acc = mapOfAccounts.get(pr.Account__c);
              Product_Registration__c aquirePr =  aquireProducts.get(pr.account__c);
              //CRM-1351 : update AEQCC's Max Airline points from Aquire's Max Airline Points
              if(aquirePr != null){
                pr.Max_Airline_Points_for_Membership_Year__c = aquirePr.Max_Airline_Points_for_Membership_Year__c;
              }
              
              //CRM-1573 always run for Amex updates when choice is 'A'  
              if((pr.Customer_s_Requested_Choice__c == 'A') || (pr.Customer_s_Requested_Choice__c != oldMap.get(pr.Id).Customer_s_Requested_Choice__c)){
                  
                  //pr.Agreed_change_date_by_Qantas__c = Date.today(); commented as part of CRM-1080
                  pr.Agreed_change_date_by_Qantas__c = pr.AMEX_Customer_Choice_Date__c;
                  
                  
                  if(acc.Agency__c == 'N' && acc.Dealing_Flag__c == 'N' && acc.Aquire_Override__c == 'N'){
                        
                        if(pr.Max_Airline_Points_for_Membership_Year__c == true && pr.CreatedDate.date() > Date.today().addYears(-1) ){
                            if(aquirePr != null && aquirePr.Membership_Expiry_Date__c != null){
                                pr.Agreed_change_date_by_Qantas__c = aquirePr.Membership_Expiry_Date__c.addDays(1);
                                system.debug('Date is changed!:'+pr.Agreed_change_date_by_Qantas__c);
                            }
                        }                            
                         if(pr.Customer_s_Requested_Choice__c == 'A'){
                            if(aquireProducts.containsKey(pr.account__c) && aquireProducts.get(pr.account__c).Stage_Status__c == 'Active'){
                              acc.Qantas_Accept_or_Reject_code__c = '0';
                              acc.AMEX_Response_Code__c = 'D';
                         // }else if(acc.Aquire__c == false){
                          }else{
                              acc.Qantas_Accept_or_Reject_code__c = '1';
                              acc.AMEX_Response_Code__c = 'R';
                          }                              
                            
                         } else if(pr.Customer_s_Requested_Choice__c == 'R'){
                            /* Uncommented for testing with one day - replace 214
                            if(pr.Max_Airline_Points_for_Membership_Year__c == true && pr.CreatedDate.date() > Date.today().addDays(-1) && pr.Membership_Expiry_Date__c != null){
                            */
                            acc.Qantas_Accept_or_Reject_code__c = '0';
                            acc.AMEX_Response_Code__c = 'R';                    
                         
                         }       
                 }else if(acc.Agency__c == 'Y'){
                    acc.Qantas_Accept_or_Reject_code__c = '2';
                    acc.AMEX_Response_Code__c = 'I';
                 }else if(acc.Dealing_Flag__c == 'Y'){
                    acc.Qantas_Accept_or_Reject_code__c = '3';
                    acc.AMEX_Response_Code__c = 'I';
                 }else if(acc.Aquire_Override__c == 'Y'){
                    /*acc.Qantas_Accept_or_Reject_code__c = '4';
                    **replaced by the condition below for CRM-1026 
                    */
                    if(pr.Customer_s_Requested_Choice__c == 'A'){
                        acc.Qantas_Accept_or_Reject_code__c = '4';
                    }else if(pr.Customer_s_Requested_Choice__c == 'R'){
                        acc.Qantas_Accept_or_Reject_code__c = '0';
                    }
                    acc.AMEX_Response_Code__c = 'R';
                 }        
              }
              updateAccounts.add(acc);
          
        }
        
        if(updateAccounts.size()>0){  
          try{
            dataBase.update(updateAccounts);
          }catch(Exception e){
             system.debug('EXCEPTION!!'+e.getStackTraceString());
          }  
       }
        system.debug ('after:'+pr.Agreed_change_date_by_Qantas__c);
        
        }      

        }
        hasRunProcessAMEXEligibilityUpdate = true;
        }
    
    /*
      Purpose:  Process AMEX eligibility on update trigger
      Parameters: trigger.new list of product Registrations, record type
      Return: list of production registration. 
    */
    private List<Product_Registration__c> getProductRegistrationRecords(String recType, List<Product_Registration__c> newList){
        
        List<Product_Registration__c> recTypeProducts = new List<Product_Registration__c>();
        String recTypeId = Schema.SObjectType.Product_Registration__c.getRecordTypeInfosByName().get(recType).getRecordTypeId();
        
        
        for(Product_Registration__c pr : newList){
            if(pr.RecordTypeId == recTypeId){
                recTypeProducts.add(pr);
            }
        }
        return recTypeProducts;
    }
    
    /*
      Purpose: Get accouts for Product registraion
      Parameters: trigger.new list of product Registrations
      Return: list of Accounts. 
    */
    private Map<Id, Account> getAccounts(List<Product_Registration__c> newList){
        List<Id> accIds = new List<Id>();
        
        for(Product_Registration__c pr : newList){
            if(pr.Account__c != null){
               accIds.add(pr.Account__c);
            }
        }
        
        Map<Id, Account> accMap = new Map<Id, Account>([SELECT Id, CreatedDate, Aquire__c, Agency__c, Dealing_Flag__c, Aquire_Override__c
                                                        FROM Account 
                                                        WHERE Id IN :accIds]);
        return accMap;
    }
    
    /*
      Purpose: Get Aquire Product registraions for an account
      Parameters: set of accountIds
      Return: Map of product registrations indexed with accountId. 
    */
    private Map<Id, Product_Registration__c> getAquireProductRegistrations(Set<id> accountIds){
        Map<Id, Product_Registration__c> aquireProducts = new Map<Id, Product_Registration__c>();
        if(accountIds.size()>0){
          String aquireRecordTypeId = Schema.SObjectType.Product_Registration__c.getRecordTypeInfosByName().get('Aquire Registration').getRecordTypeId();
          for(Product_Registration__c pr : [SELECT Id, Account__c, Stage_Status__c,Membership_Expiry_Date__c ,Max_Airline_Points_for_Membership_Year__c
                                            FROM Product_Registration__c 
                                            WHERE RecordTypeId =:aquireRecordTypeId AND Account__c IN :accountIds]){
              aquireProducts.put(pr.Account__c, pr);
          }
        }
        return aquireProducts;
    }
    /*
      Purpose: Remove all special characters from Spend Amount.
    */
    private String rectifySpendAmount(String str){
        String regExp = '^((0+)?(\\D)+(0+)?|0+)';
        String amount = str.replaceFirst(regExp, '');
        if(amount.contains(',')) amount = amount.remove(','); 
        return amount;
    }

}