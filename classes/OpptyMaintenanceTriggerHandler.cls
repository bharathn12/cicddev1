/****************************************************************************************************************************************************************************
 
    Created Date  : 31/08/16 (DD/MM/YYYY)
    Added By      : Vinothkumar Balasubramanian
    Description   : Trigger Handler Class to update the Custom Settings "Opportunity maintenance" while updating or Inserting the Active QBS or CA Opportunity maintance records
    JIRA          : CRM - 2053, CRM - 2217 and CRM - 2238
    Version       :V1.0 - 31/08/16 - Initial version
 
*****************************************************************************************************************************************************************************/

               /*
                This method is to update the fields "Close Date"
                 and "Create an Opportunity before (in months)" on the Custom settings "Opportunity maintenance"
                */



public class OpptyMaintenanceTriggerHandler {
    
   
        
    public static void updateclosurecreationdate(List<Opportunity_Maintenance__c> newList, Map<Id, Opportunity_Maintenance__c> oldMap)
    
    {
              Opportunity_main__c opptymain = Opportunity_main__c.getValues('Corporate Airfares');
        Opportunity_main__c opptymain1 = Opportunity_main__c.getValues('Qantas Business Savings');
        
        List<Opportunity_main__c> opptymain3 = new List<Opportunity_main__c>();
        List<Opportunity_main__c> opptymain4 = new List<Opportunity_main__c>();
        
        Set<Id> setOpptymaintenanceChangedIds = new Set<Id>();
        
         for(Opportunity_Maintenance__c Oopptymain: newList){
             
              boolean bIsChanged = Oopptymain.Create_an_Opportunity_before_in_months__c != oldMap.get(Oopptymain.Id).Create_an_Opportunity_before_in_months__c
         || Oopptymain.Create_an_Opportunity_before_in_months__c == oldMap.get(Oopptymain.Id).Create_an_Opportunity_before_in_months__c ||
          Oopptymain.Close_Date__c != oldMap.get(Oopptymain.Id).Close_Date__c ||
           Oopptymain.Close_Date__c == oldMap.get(Oopptymain.Id).Close_Date__c ||
         Oopptymain.Active__c != oldMap.get(Oopptymain.Id).Active__c || Oopptymain.Active__c == oldMap.get(Oopptymain.Id).Active__c;
          
             if (bIsChanged) {
                setOpptymaintenanceChangedIds.add(Oopptymain.Id);
            }
            
           if (setOpptymaintenanceChangedIds.isEmpty() == false)
    {
        
        List<Opportunity_Maintenance__c> listopptymain = [SELECT Id, Account_Record_Type__c, Active__c,Category__c, Create_an_Opportunity_before_in_months__c, Close_Date__c
        FROM Opportunity_Maintenance__c WHERE Id IN :setOpptymaintenanceChangedIds];
        
     for (Opportunity_Maintenance__c opptymain2 : listopptymain)
     {
         if (opptymain2.Category__c == 'Corporate Airfares'){
             
             if (opptymain2.Active__c == true){
             
         opptymain.Create_an_Opportunity_before_in_months__c = opptymain2.Create_an_Opportunity_before_in_months__c;
         opptymain.Close_Date__c = opptymain2.Close_Date__c;
         opptymain.Active__c = opptymain2.Active__c;
         
          }
          
          else
          {
              opptymain.Create_an_Opportunity_before_in_months__c = NULL;
              opptymain.Close_Date__c = NULL;
               opptymain.Active__c = opptymain2.Active__c;
          }
          
          System.Debug('Corp Create on Opportunity before in months (after update) ' +opptymain.Create_an_Opportunity_before_in_months__c);
          System.Debug('Corp Close Date (after update) ' +opptymain.Close_Date__c);
          System.Debug('Corp Active (after update) ' +opptymain.Active__c);
         
          opptymain3.add(opptymain);
          
     }
         
        
         
         if (opptymain2.Category__c == 'Qantas Business Savings'){
             
              if (opptymain2.Active__c == true)
         
         {
         opptymain1.Create_an_Opportunity_before_in_months__c = opptymain2.Create_an_Opportunity_before_in_months__c;
          opptymain1.Close_Date__c = opptymain2.Close_Date__c;
          opptymain1.Active__c = opptymain2.Active__c;
         
       
          }
         else
         {
            opptymain1.Create_an_Opportunity_before_in_months__c = NULL;
            opptymain1.Close_Date__c = NULL;
            opptymain1.Active__c = opptymain2.Active__c;
             
         }
         
            System.Debug('QBS Create on Opportunity before in months (after update) ' +opptymain1.Create_an_Opportunity_before_in_months__c);
            System.Debug('QBS Close Date (after update)  ' +opptymain1.Close_Date__c);
            System.Debug('QBS Active (after update) ' +opptymain1.Active__c);
         opptymain4.add(opptymain1);
         
         }
   
 update opptymain3;
 update opptymain4;
}
        }
        
    }
    }
    
                /*
                This method is to insert the values on the fields "Expected Closure Date"
                and "Create an Opportunity before (in months)" on the Custom settings "Opportunity maintenance"
                once the Active Opportunity maintenance record is created
                 
                */
    
    public static void insertclosurecreationdate(List<Opportunity_Maintenance__c> newList)
    {
        
        Opportunity_main__c opptymain = Opportunity_main__c.getValues('Corporate Airfares');
        Opportunity_main__c opptymain1 = Opportunity_main__c.getValues('Qantas Business Savings');
        List<Opportunity_main__c> opptymain8 = new List<Opportunity_main__c>();
        List<Opportunity_main__c> opptymain9 = new List<Opportunity_main__c>();
        
        for (Opportunity_Maintenance__c opptymain5 : newList)
           {
               if(opptymain5.Category__c == 'Corporate Airfares')
               {
               if(opptymain5.Active__c == true){
         opptymain.Create_an_Opportunity_before_in_months__c = opptymain5.Create_an_Opportunity_before_in_months__c;
         opptymain.Close_Date__c = opptymain5.Close_Date__c;
         opptymain.Active__c = opptymain5.Active__c;
               }
               
               else
               {
         opptymain.Create_an_Opportunity_before_in_months__c = NULL;
         opptymain.Close_Date__c = NULL;
         opptymain.Active__c = opptymain5.Active__c;
               }
         
          System.Debug('Corp Create on Opportunity before in months (after insert) ' +opptymain.Create_an_Opportunity_before_in_months__c );
          System.Debug('Corp Close Date (after insert) ' +opptymain.Close_Date__c );
          System.Debug('Corp Active(after insert) ' +opptymain.Active__c );
          
         opptymain8.add(opptymain);
               }
                   if(opptymain5.Category__c == 'Qantas Business Savings')
                   {
                   if(opptymain5.Active__c == true){
         opptymain1.Create_an_Opportunity_before_in_months__c = opptymain5.Create_an_Opportunity_before_in_months__c;
         opptymain1.Close_Date__c = opptymain5.Close_Date__c;
         opptymain1.Active__c = opptymain5.Active__c;
                   }
                   
                      else
               {
         opptymain1.Create_an_Opportunity_before_in_months__c = NULL;
         opptymain1.Close_Date__c = NULL;
         opptymain1.Active__c = opptymain5.Active__c;
               }
                   
                   
         
          System.Debug('QBS Create on Opportunity before in months (after insert) ' +opptymain1.Create_an_Opportunity_before_in_months__c );
          System.Debug('QBS Close Date (after insert) ' +opptymain1.Close_Date__c );
          System.Debug('QBS Active (after insert) ' +opptymain1.Active__c);
         opptymain9.add(opptymain1);
           }   
           
        
 update opptymain8;
 update opptymain9;
}
    }
    
}