/**********************************************************************************************************************************
 
    Created Date: 10/03/2015
 
    Description: Process Account trigger for:
                 1. Assign account team members based on user department after insert.
                 2. Opportunity creation based on travel frequency before insert.
  
    Versión:
    V1.0 - 10/03/2015 - Initial version [FO]
    V2.0 - 19/10/2015 - Update the logic to run only for QIS team CRM-1677
    V3.0 - 23/10/2015 - remove the logic for QIS team CRM-1677  
    V4.0 - 28/10/2015 - make 'Marketing Acquisition & Sales Team' assignemnts for prospect accounts only     
**********************************************************************************************************************************/
public class AccountTeamHandler {
    
    static String prospectRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
    static String customerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();    
   
    /*
      Purpose:  Remove existing Account team members.
      Parameters: Trigger.new - list of Accounts, Trigger.oldMap - list of Accounts
      Return: none. 
    */
    public void removeAccountTeam(List<Account> newList, Map<Id, Account> oldMap){
        
        Set<id> accIds = new Set<id>();

        for(Account acc : newList){
            if(acc.Estimated_Total_Air_Travel_Spend__c != oldMap.get(acc.id).Estimated_Total_Air_Travel_Spend__c){
                accIds.add(acc.id);
            }
        }

        List<AccountTeamMember> accountTeamMembers = new List<AccountTeamMember>();

        if(accIds != null && accIds.size() > 0) accountTeamMembers = [SELECT id FROM AccountTeamMember WHERE AccountId IN:accIds];

        if(accountTeamMembers != null && accountTeamMembers.size() > 0){
            try{ 
                Database.delete(accountTeamMembers);
            }catch(Exception e){
                system.debug('EXCEPTION!!'+e.getStackTraceString());
            }
        }
        
    }
    
   /*
      Purpose:  Assign Account team members based on user department, accounts from source Aquire and of type Prospect Account.
      Parameters: Trigger.new - list of Accounts, Trigger.oldMap - list of Accounts
      Return: none. 
    */
    public void createAccountTeamMembers(List<Account> newList, Map<Id, Account> oldMap){
        
        
        //newList = filterByOwnerRole(newList);
        if(newList.size()>0){
            removeAccountTeam(newList, oldMap);
            
            Map<String, List<Account>> aquireTeamGroup;
            List<AccountTeamMember> newAccountTeamMembers = new List<AccountTeamMember>();
            AccountTeamMember accTeamMember; 
            aquireTeamGroup = this.groupAccountsByRevenue(newList, oldMap); 
            
            if(aquireTeamGroup.containsKey('SBD Team')){
                newAccountTeamMembers = accountTeamHandling(aquireTeamGroup.get('SBD Team'));
                aquireTeamGroup.remove('SBD Team');
            }
              
            for(user teamMember :getTeamMembersByDepartment(aquireTeamGroup.keySet())){
                  system.debug('User:'+teamMember);
                  for(Account acc : aquireTeamGroup.get(teamMember.Department)) {
                        system.debug('Account owner before:'+acc);
                        if(teamMember.Department_Lead__c == true){
                            acc.OwnerId = teamMember.id;
                        }
                  
                        accTeamMember = new AccountTeamMember();
                        accTeamMember.AccountId = acc.id;
                        accTeamMember.UserId = teamMember.id;
                        accTeamMember.TeamMemberRole = teamMember.Department;
                        newAccountTeamMembers.add(accTeamMember);
                        system.debug('Account owner after:'+acc);
                  }
                  
            }
            /* Save account team members*/
            if(newAccountTeamMembers.size() > 0){
                  
                  try{
                    Database.insert(newAccountTeamMembers);
                  }catch(Exception e){
                    system.debug('EXCEPTION!!'+e.getStackTraceString());
                  }
            }
        }
    }
    
  /*
      Purpose:  Filter Accounts by User Roles in custom setting.
      Parameters: Trigger.new - list of Accounts
      Return: List of Accounts. 
  */   
    private List<Account> filterByOwnerRole(List<Account> newList){
        List<Account> filteredList = new List<Account>();
        Set<Id> allOwnerIds = new Set<Id>();
        for(Account acc : newList){
            allOwnerIds.add(acc.OwnerId);
        }
        
        Map<Id, User> userMap = new Map<Id, User>([SELECT Id, UserRole.Name FROM USER where Id IN :allOwnerIds]);       
        
        if(userMap.size() > 0){
            for(Account acc : newList){
                if(acc.Manual_Revenue_Update__c != true || (acc.Manual_Revenue_Update__c == true && userMap.get(acc.OwnerId).UserRole.Name !=null && Assignment_User_Roles__c.getInstance(userMap.get(acc.OwnerId).UserRole.Name) != null)){
                    filteredList.add(acc);
                }
            }
        }
        return filteredList;    
    }
    
    /*
      Purpose:  Group accounts by annual revenue range and user department.
      Parameters: Trigger.new - list of Accounts, Trigger.oldMap - list of Accounts
      Return: Map of Accounts. 
    */
    private Map<String, List<Account>> groupAccountsByRevenue(List<Account> newList, Map<Id, Account> oldMap){
        
        Set<id> accIds = new Set<id>();
        
        for(Account acc : newList){
            if(acc.Estimated_Total_Air_Travel_Spend__c!= oldMap.get(acc.id).Estimated_Total_Air_Travel_Spend__c || acc.Estimated_Total_Air_Travel_Spend__c == null){
                accIds.add(acc.id);
            }
        }
        
        List<Account_Revenue__c> customSettingList;
        if(accIds.size() > 0) customSettingList = [SELECT Name, Maximum__c, Minimum__c FROM Account_Revenue__c];
        System.debug('***'+newList); 
        Map<String, List<Account>> aquireGroup = new  Map<String, List<Account>>();
        system.debug('before aquireGroup'+aquireGroup);
        for(Account acc : newList){
            system.debug(acc);
            if(acc.Estimated_Total_Air_Travel_Spend__c == oldMap.get(acc.id).Estimated_Total_Air_Travel_Spend__c && acc.Estimated_Total_Air_Travel_Spend__c != null){
                continue;
            }
            if(acc.Aquire__c == true && (acc.RecordTypeId == AccountTeamHandler.prospectRecordTypeId ) ){//CRM-1677 || acc.RecordTypeId == AccountTeamHandler.customerRecordTypeId)){
                system.debug('in Aquire if:'+acc);    
                for(Account_Revenue__c ar : customSettingList){
                    
                    if(acc.Estimated_Total_Air_Travel_Spend__c >= ar.Minimum__c.intValue() && acc.Estimated_Total_Air_Travel_Spend__c <= ar.Maximum__c.intValue()){
                    
                        if(!aquireGroup.containsKey(ar.Name)){
                            aquireGroup.put(ar.Name, new list<Account>());
                        }
                        system.debug('acc belongs to aquiregroup'+acc);    
                        aquireGroup.get(ar.Name).add(acc);    
                    }
                }              
                if(acc.Estimated_Total_Air_Travel_Spend__c >= 300000 && acc.Estimated_Total_Air_Travel_Spend__c < 1000000){
                   if(!aquireGroup.containsKey('SBD Team')){
                      aquireGroup.put('SBD Team', new list<Account>());
                   }
                   system.debug('acc belongs to SBD'+acc);  
                   aquireGroup.get('SBD Team').add(acc);
                }
                if(acc.Estimated_Total_Air_Travel_Spend__c == null){
                   if(!aquireGroup.containsKey('Marketing Acquisition & Sales Team')){
                      aquireGroup.put('Marketing Acquisition & Sales Team', new list<Account>());
                   }
                   system.debug('acc belongs to MAST since rev is null'+acc);  
                   aquireGroup.get('Marketing Acquisition & Sales Team').add(acc);
                }            
                
            }else if(acc.AMEX__c == true && (acc.RecordTypeId == AccountTeamHandler.prospectRecordTypeId ) ){ 
                if(!aquireGroup.containsKey('Marketing Acquisition & Sales Team')){
                    aquireGroup.put('Marketing Acquisition & Sales Team', new list<Account>());
                }
                system.debug('acc belongs to MAST since has amex'+acc);  
                aquireGroup.get('Marketing Acquisition & Sales Team').add(acc);
            }
        }
        system.debug('after aquireGroup'+aquireGroup);
        return aquireGroup;
    }
    
    /*
      Purpose:  Handle AccountTeamMember of Account revenue fall in 300K - 1M.
      Parameters: Trigger.new - list of Accounts.
      Return: list of AccountTeamMember. 
    */
    public List<AccountTeamMember> accountTeamHandling(List<Account> accList){
         system.debug('Begin Account assignemnt for SBD ');
         List<AccountTeamMember> listAccTeam = new List<AccountTeamMember>();
         List<Account_Owner__c> listAO = [SELECT Name,Account_Manager__c, Business_Development_Rep__c, Min_Revenue__c, Max_Revenue__c 
                                          FROM Account_Owner__c ];
                  
         for(Account acc : accList){
                system.debug('Account before Owner'+acc);             
                String name = acc.Name.subString(0,1);
                
                for(Account_Owner__c ao : listAO){
                
                   if(ao.Name.contains(name)){
                                              
                           acc.OwnerId = ao.Account_Manager__c;
                           
                           AccountTeamMember member = new AccountTeamMember();
                           member.AccountId = acc.Id;
                           member.UserId = ao.Account_Manager__c;
                           member.TeamMemberRole = 'Account Manager';
                           listAccTeam.add(member);
                                                      
                           AccountTeamMember member1 = new AccountTeamMember();
                           member1.AccountId = acc.Id;
                           member1.UserId = ao.Business_Development_Rep__c;
                           member1.TeamMemberRole = 'Business Development Rep';
                           listAccTeam.add(member1);                                              
                        
                  } 
                }           
            system.debug('Account after Owner'+acc);             
         }
        system.debug('END  Account assignemnt for SBD '+listAccTeam);   
        return listAccTeam;    
    }

   /*
      Purpose:  Set Account Anual Revenue Manual Update flag.
      Parameters: Trigger.new - list of Accounts, Trigger.oldMap - list of Accounts
      Return: list of AccountTeamMember. 
    */
   public void setRevenueManualUpdate(List<Account> newList, Map<Id, Account> oldMap){
        
        map<id, Product_Registration__c> pRegisters = new map<id, Product_Registration__c>();
        List<Id> accIds = new List<Id>();
        for(Account acc : newList){
            if(acc.Estimated_Total_Air_Travel_Spend__c != oldMap.get(acc.id).Estimated_Total_Air_Travel_Spend__c)
                accIds.add(acc.Id);
        }
        if(accIds.size()>0){
            for(Product_Registration__c pr:[SELECT Account__c, Estimated_Total_Annual_Revenue__c, LastModifiedById 
                                            FROM Product_Registration__c 
                                            WHERE Account__c IN:accIds]){
                pRegisters.put(pr.Account__c, pr);
            }
        }
        
        for(Account acc: newList){
            if(!pRegisters.containsKey(acc.id)){
                continue;
            }
            
            String currentUserId = UserInfo.getUserId();
            system.debug('beforemanual'+acc);
            if((acc.Manual_Revenue_Update__c == false) && (acc.Estimated_Total_Air_Travel_Spend__c != oldMap.get(acc.id).Estimated_Total_Air_Travel_Spend__c)
                                                       && (acc.Estimated_Total_Air_Travel_Spend__c != pRegisters.get(acc.id).Estimated_Total_Annual_Revenue__c)
                                                       && (currentUserId != pRegisters.get(acc.id).LastModifiedById)){
                acc.Manual_Revenue_Update__c = true;
                
           }
           system.debug('aftermanual'+acc);
        }
    
   }
   
    /*
    * Method to form Map of old Accounts
   
    public Map<Id, Account> getOldAccMap(List<Account> oldList){
        
        Map<Id, Account> oldAccMap = new Map<Id, Account>();
        
        for(Account acc : oldList){
            oldAccMap.put(acc.Id, acc);
        }
        return oldAccMap;        
    }
    */
    /*
      Purpose:  Fetch users by department.
      Parameters: set of department values
      Return: list of users. 
    */
    private list<user> getTeamMembersByDepartment(set<string> departments){
        List<User> userList = new List<User>();
        if(departments.size() > 0){
            userList = [SELECT id, Department, Department_Lead__c FROM user WHERE isActive = true AND Department IN:departments];
        }
        return userList;
    }
}