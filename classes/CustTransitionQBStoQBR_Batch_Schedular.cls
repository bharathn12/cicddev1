/**********************************************************************************************************************************
 
    Created Date     :  24/04/17 (DD/MM/YYYY)
    Added By         : Karpagam Swaminathan
    Description      : Schedular Class to run the Batch class "CustTransitionQBStoQBR_Batch" daily at 08.00 PM AEST (Australian Time)
    JIRA             : CRM-2522
    Version          :V1.0 - 24/04/17 - Initial version
 
**********************************************************************************************************************************/




global class CustTransitionQBStoQBR_Batch_Schedular implements Schedulable {

        global void execute(SchedulableContext schedulableContext) {
        CustTransitionQBStoQBR_Batch cust = new CustTransitionQBStoQBR_Batch();
        Database.executeBatch(cust,1);
        }
}