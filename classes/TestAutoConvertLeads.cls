@isTest(SeeAllData = true)
private class TestAutoConvertLeads {
    private static Id leadId;
    private static Integer LEAD_COUNT = 0;

    static testMethod void Test_LeadConvert() {
        LEAD_COUNT += 1;
        Profile pf = [Select Id from Profile where Name = 'System Administrator'];
        User u = new User();
        u.FirstName = 'Test';
        u.LastName = 'User';
        u.Email = 'testuser@test123456789.com';
        u.CompanyName = 'test.com';
        u.Title = 'Test User';
        u.Username = 'testuser@test123456789.com';
        u.Alias = 'testuser';
        u.CommunityNickname = 'Test User';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = pf.Id;
        u.LanguageLocaleKey = 'en_US';
        insert u;

        system.runAs(u) {

            String prospectRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
            Account acc = new Account(Name = 'Sample', Active__c = true, Aquire__c = true, Type = 'Prospect Account', RecordTypeId = prospectRecordTypeId, Migrated__c = false, Estimated_Total_Air_Travel_Spend__c = 0, Manual_Revenue_Update__c = false, Agency__c = 'N', Dealing_Flag__c = 'N', Aquire_Override__c = 'N', Contract_End_Date__c = Date.Today());

            insert acc;
            Contact con = new Contact(FirstName = 'Sample', LastName = acc.Name, AccountId = acc.Id, Email = 'abc453422@xyz.com');

            insert con;
            Sales_Region__c sr = new Sales_Region__c(Name = 'AD - Agency Development Account', sales_h_eq_type__c = 'Industry', sales_h_type__c = 'Industry');
            insert sr;

            Lead testLead = new Lead();
            testLead.FirstName = 'Test First1' + LEAD_COUNT;
            testLead.LastName = 'Test Last1' + LEAD_COUNT;
            testLead.Company = 'Test Co [VIC]';
            testLead.ABN_Tax_Reference__c = '23232323231';
            testLead.Status = 'Qualified';
            testLead.Company = 'Appshark';
            testLead.Account_Tier__c = 'Industry - Platinum';
            testLead.Agency_TIDS__c = '43991';
            testLead.Business_Type__c = 'Agency';
            testLead.Estimated_Total_Air_Travel_Spend__c = 100;
            testLead.Frequent_Flyer_Number__c = '123';
            testLead.Function__c = 'Finance';
            testLead.IATA__c = '8764';
            testLead.Job_Role__c = 'Agency Owner';
            testLead.Job_Title__c = 'Mr';
            testLead.Legal_Name__c = 'Test ABC';
            testLead.New_Account_Type__c = 'Agency';
            testLead.Data_Quality_Score_for_All__c = 100;
            testLead.Street = 'Test';
            testLead.CountryCode = 'AU';
            testLead.City = 'Sydney';
            testLead.State = 'VIC';
            testLead.Postalcode = '2060';
            testLead.Salutation = 'Mr.';
            testLead.Email = 'abc453422@xyz.com';
            testLead.Phone = '123456';
            testLead.Frequent_Flyer_Number__c = '64322';
            testLead.Business_Type__c = 'Agency';
            testLead.Job_Role__c = 'Agency Manager';
            testLead.Function__c = 'Finance';
            testLead.IATA__c = '41373';
            testLead.Agency_TIDS__c = '41373';
            testLead.Account_Tier__c = 'Industry - Gold';
            testLead.Estimated_Total_Air_Travel_Spend__c = 110;
            testLead.NumberOfEmployees = 100;
            testLead.AnnualRevenue = 1000;
            testLead.LeadSource = 'Web';
            testLEad.Industry = 'Manufacturing';
            testLead.Website = 'www.test.com';
            testLead.MobilePhone = '9999999';
            testLead.Agency_Sales_Region__c = sr.id;
            testLead.Parent_Account__c = acc.id;

            Test.startTest();
            insert testLead;
            leadId = testLead.Id;
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(LeadId);
            LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted = true limit 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);

            //   Database.LeadConvertResult lcr = Database.convertLead(lc);
            //   System.assert(lcr.isSuccess());
            Test.stopTest();

        }

    }
    static testMethod void Test_LeadConvertForCustomerAcc() {
        LEAD_COUNT += 1;

        Profile pf = [Select Id from Profile where Name = 'System Administrator'];
        User u = new User();
        u.FirstName = 'Test';
        u.LastName = 'User';
        u.Email = 'testuser@test123456789.com';
        u.CompanyName = 'test.com';
        u.Title = 'Test User';
        u.Username = 'testuser@test123456789.com';
        u.Alias = 'testuser';
        u.CommunityNickname = 'Test User';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = pf.Id;
        u.LanguageLocaleKey = 'en_US';
        insert u;

        system.runAs(u) {

            // TestUtilityDataClassQantas.enableTriggers();
            String prospectRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
            Account acc = new Account(Name = 'Sample', Active__c = true, Aquire__c = true, Type = 'Prospect Account', RecordTypeId = prospectRecordTypeId, Migrated__c = false, Estimated_Total_Air_Travel_Spend__c = 0, Manual_Revenue_Update__c = false, Agency__c = 'N', Dealing_Flag__c = 'N', Aquire_Override__c = 'N', Contract_End_Date__c = Date.Today());

            insert acc;
            Sales_Region__c sr = new Sales_Region__c(Name = 'AD - Agency Development Account', sales_h_eq_type__c = 'Industry', sales_h_type__c = 'Industry');
            insert sr;

            Lead testLead = new Lead();
            testLead.FirstName = 'Test First1' + LEAD_COUNT;
            testLead.LastName = 'Test Last1' + LEAD_COUNT;
            testLead.Company = 'Test Co';
            testLead.ABN_Tax_Reference__c = '23232323231';
            testLead.Status = 'Qualified';
            testLead.Company = 'Appshark';
            testLead.Account_Tier__c = 'Industry - Platinum';
            testLead.Agency_TIDS__c = '43991';
            testLead.Business_Type__c = 'Customer';
            testLead.Estimated_Total_Air_Travel_Spend__c = 100;
            testLead.Frequent_Flyer_Number__c = '123';
            testLead.Function__c = 'Finance';
            testLead.IATA__c = '8764';
            testLead.Job_Role__c = 'Agency Owner';
            testLead.Job_Title__c = 'Mr';
            testLead.Legal_Name__c = 'Test ABC';
            testLead.New_Account_Type__c = 'Customer';
            testLead.Data_Quality_Score_for_All__c = 100;
            testLead.Street = 'Test';
            testLead.CountryCode = 'AU';
            testLead.City = 'Sydney';
            testLead.State = 'VIC';
            testLead.Postalcode = '2060';
            // testLead.FirstName='Joihn1';
            // testLead.LastName='Smiith1';
            testLead.Salutation = 'Mr.';
            testLead.Email = 'abc453422@xyz.com';
            testLead.Phone = '123456';
            testLead.Frequent_Flyer_Number__c = '64322';
            testLead.Business_Type__c = 'Agency';
            testLead.Job_Role__c = 'Agency Manager';
            testLead.Function__c = 'Finance';
            testLead.IATA__c = '41373';
            testLead.Agency_TIDS__c = '41373';
            testLead.Account_Tier__c = 'Industry - Gold';
            testLead.Estimated_Total_Air_Travel_Spend__c = 110;
            testLead.NumberOfEmployees = 100;
            testLead.AnnualRevenue = 1000;
            testLead.LeadSource = 'Web';
            testLEad.Industry = 'Manufacturing';
            testLead.Website = 'www.test.com';
            testLead.MobilePhone = '9999999';
            testLead.Agency_Sales_Region__c = sr.id;
            testLead.Parent_Account__c = acc.id;

            Test.startTest();
            insert testLead;

            leadId = testLead.Id;
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(LeadId);

            LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted = true limit 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);

            //   Database.LeadConvertResult lcr = Database.convertLead(lc);
            //   System.assert(lcr.isSuccess());
            Test.stopTest();

        }

    }
    static testMethod void Test_LeadConvertForProspectAcc() {
        LEAD_COUNT += 1;

        Profile pf = [Select Id from Profile where Name = 'System Administrator'];
        User u = new User();
        u.FirstName = 'Test';
        u.LastName = 'User';
        u.Email = 'testuser@test123456789.com';
        u.CompanyName = 'test.com';
        u.Title = 'Test User';
        u.Username = 'testuser@test123456789.com';
        u.Alias = 'testuser';
        u.CommunityNickname = 'Test User';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = pf.Id;
        u.LanguageLocaleKey = 'en_US';
        insert u;

        system.runAs(u) {

            // TestUtilityDataClassQantas.enableTriggers();
            String prospectRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
            Account acc = new Account(Name = 'Sample', Active__c = true, Aquire__c = true, Type = 'Prospect Account', RecordTypeId = prospectRecordTypeId, Migrated__c = false, Estimated_Total_Air_Travel_Spend__c = 0, Manual_Revenue_Update__c = false, Agency__c = 'N', Dealing_Flag__c = 'N', Aquire_Override__c = 'N', Contract_End_Date__c = Date.Today());

            insert acc;
            Sales_Region__c sr = new Sales_Region__c(Name = 'AD - Agency Development Account', sales_h_eq_type__c = 'Industry', sales_h_type__c = 'Industry');
            insert sr;

            Lead testLead = new Lead();
            testLead.FirstName = 'Test First1' + LEAD_COUNT;
            testLead.LastName = 'Test Last1' + LEAD_COUNT;
            testLead.Company = 'Test Co';
            testLead.ABN_Tax_Reference__c = '23232323231';
            testLead.Status = 'Qualified';
            testLead.Company = 'Appshark';
            testLead.Account_Tier__c = 'Industry - Platinum';
            testLead.Agency_TIDS__c = '43991';
            testLead.Business_Type__c = 'Agency';
            testLead.Estimated_Total_Air_Travel_Spend__c = 100;
            testLead.Frequent_Flyer_Number__c = '123';
            testLead.Function__c = 'Finance';
            testLead.IATA__c = '8764';
            testLead.Job_Role__c = 'Agency Owner';
            testLead.Job_Title__c = 'Mr';
            testLead.Legal_Name__c = 'Test ABC';
            testLead.New_Account_Type__c = 'Prospect';
            testLead.Data_Quality_Score_for_All__c = 100;
            testLead.Street = 'Test';
            testLead.CountryCode = 'AU';
            testLead.City = 'Sydney';
            testLead.State = 'VIC';
            testLead.Postalcode = '2060';
            // testLead.FirstName='Joihn1';
            // testLead.LastName='Smiith1';
            testLead.Salutation = 'Mr.';
            testLead.Email = 'abc453422@xyz.com';
            testLead.Phone = '123456';
            testLead.Frequent_Flyer_Number__c = '64322';
            testLead.Business_Type__c = 'Agency';
            testLead.Job_Role__c = 'Agency Manager';
            testLead.Function__c = 'Finance';
            testLead.IATA__c = '41373';
            testLead.Agency_TIDS__c = '41373';
            testLead.Account_Tier__c = 'Industry - Gold';
            testLead.Estimated_Total_Air_Travel_Spend__c = 110;
            testLead.NumberOfEmployees = 100;
            testLead.AnnualRevenue = 1000;
            testLead.LeadSource = 'Web';
            testLEad.Industry = 'Manufacturing';
            testLead.Website = 'www.test.com';
            testLead.MobilePhone = '9999999';
            testLead.Agency_Sales_Region__c = sr.id;
            testLead.Parent_Account__c = acc.id;

            Test.startTest();
            insert testLead;

            leadId = testLead.Id;
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(LeadId);

            LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted = true limit 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);

            //   Database.LeadConvertResult lcr = Database.convertLead(lc);
            //   System.assert(lcr.isSuccess());
            Test.stopTest();

        }

    }
    static testMethod void Test_LeadConvertForNonAUAcc() {
        LEAD_COUNT += 1;

        Profile pf = [Select Id from Profile where Name = 'System Administrator'];
        User u = new User();
        u.FirstName = 'Test';
        u.LastName = 'User';
        u.Email = 'testuser@test123456789.com';
        u.CompanyName = 'test.com';
        u.Title = 'Test User';
        u.Username = 'testuser@test123456789.com';
        u.Alias = 'testuser';
        u.CommunityNickname = 'Test User';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = pf.Id;
        u.LanguageLocaleKey = 'en_US';
        insert u;

        system.runAs(u) {

            // TestUtilityDataClassQantas.enableTriggers();
            String prospectRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
            Account acc = new Account(Name = 'Sample', Active__c = true, Aquire__c = true, Type = 'Prospect Account', RecordTypeId = prospectRecordTypeId, Migrated__c = false, Estimated_Total_Air_Travel_Spend__c = 0, Manual_Revenue_Update__c = false, Agency__c = 'N', Dealing_Flag__c = 'N', Aquire_Override__c = 'N', Contract_End_Date__c = Date.Today());

            insert acc;
            Sales_Region__c sr = new Sales_Region__c(Name = 'AD - Agency Development Account', sales_h_eq_type__c = 'Industry', sales_h_type__c = 'Industry');
            insert sr;

            Lead testLead = new Lead();
            testLead.FirstName = 'Test First1' + LEAD_COUNT;
            testLead.LastName = 'Test Last1' + LEAD_COUNT;
            testLead.Company = 'Test Co';
            testLead.ABN_Tax_Reference__c = '23232323231';
            testLead.Status = 'Qualified';
            testLead.Company = 'Appshark';
            testLead.Account_Tier__c = 'Industry - Platinum';
            testLead.Agency_TIDS__c = '43991';
            testLead.Business_Type__c = 'Agency';
            testLead.Estimated_Total_Air_Travel_Spend__c = 100;
            testLead.Frequent_Flyer_Number__c = '123';
            testLead.Function__c = 'Finance';
            testLead.IATA__c = '8764';
            testLead.Job_Role__c = 'Agency Owner';
            testLead.Job_Title__c = 'Mr';
            testLead.Legal_Name__c = 'Test ABC';
            testLead.New_Account_Type__c = 'Agency';
            testLead.Data_Quality_Score_for_All__c = 100;
            testLead.Street = 'Test';
            testLead.CountryCode = 'US';
            testLead.City = 'Dallas';
            //testLead.State='VIC';
            testLead.Postalcode = '2060';
            // testLead.FirstName='Joihn1';
            // testLead.LastName='Smiith1';
            testLead.Salutation = 'Mr.';
            testLead.Email = 'abc453422@xyz.com';
            testLead.Phone = '123456';
            testLead.Frequent_Flyer_Number__c = '64322';
            testLead.Business_Type__c = 'Agency';
            testLead.Job_Role__c = 'Agency Manager';
            testLead.Function__c = 'Finance';
            testLead.IATA__c = '41373';
            testLead.Agency_TIDS__c = '41373';
            testLead.Account_Tier__c = 'Industry - Gold';
            testLead.Estimated_Total_Air_Travel_Spend__c = 110;
            testLead.NumberOfEmployees = 100;
            testLead.AnnualRevenue = 1000;
            testLead.LeadSource = 'Web';
            testLEad.Industry = 'Manufacturing';
            testLead.Website = 'www.test.com';
            testLead.MobilePhone = '9999999';
            testLead.Agency_Sales_Region__c = sr.id;
            testLead.Parent_Account__c = acc.id;

            Test.startTest();
            insert testLead;

            leadId = testLead.Id;
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(LeadId);

            LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted = true limit 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);

            //   Database.LeadConvertResult lcr = Database.convertLead(lc);
            //   System.assert(lcr.isSuccess());
            Test.stopTest();

        }

    }
    static testMethod void Test_LeadConvertForCharterAUAcc() {
        LEAD_COUNT += 1;

        Profile pf = [Select Id from Profile where Name = 'System Administrator'];
        User u = new User();
        u.FirstName = 'Test';
        u.LastName = 'User';
        u.Email = 'testuser@test123456789.com';
        u.CompanyName = 'test.com';
        u.Title = 'Test User';
        u.Username = 'testuser@test123456789.com';
        u.Alias = 'testuser';
        u.CommunityNickname = 'Test User';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = pf.Id;
        u.LanguageLocaleKey = 'en_US';
        insert u;

        system.runAs(u) {

            // TestUtilityDataClassQantas.enableTriggers();
            String prospectRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
            Account acc = new Account(Name = 'Sample', Active__c = true, Aquire__c = true, Type = 'Prospect Account', RecordTypeId = prospectRecordTypeId, Migrated__c = false, Estimated_Total_Air_Travel_Spend__c = 0, Manual_Revenue_Update__c = false, Agency__c = 'N', Dealing_Flag__c = 'N', Aquire_Override__c = 'N', Contract_End_Date__c = Date.Today());

            insert acc;
            Sales_Region__c sr = new Sales_Region__c(Name = 'AD - Agency Development Account', sales_h_eq_type__c = 'Industry', sales_h_type__c = 'Industry');
            insert sr;

            Lead testLead = new Lead();
            testLead.FirstName = 'Test First1' + LEAD_COUNT;
            testLead.LastName = 'Test Last1' + LEAD_COUNT;
            testLead.Company = 'Test Co';
            testLead.ABN_Tax_Reference__c = '23232323231';
            testLead.Status = 'Qualified';
            testLead.Company = 'Appshark';
            testLead.Account_Tier__c = 'Industry - Platinum';
            testLead.Agency_TIDS__c = '43991';
            testLead.Business_Type__c = 'Agency';
            testLead.Estimated_Total_Air_Travel_Spend__c = 100;
            testLead.Frequent_Flyer_Number__c = '123';
            testLead.Function__c = 'Finance';
            testLead.IATA__c = '8764';
            testLead.Job_Role__c = 'Agency Owner';
            testLead.Job_Title__c = 'Mr';
            testLead.Legal_Name__c = 'Test ABC';
            testLead.New_Account_Type__c = 'Charter';
            testLead.Data_Quality_Score_for_All__c = 100;
            testLead.Street = 'Test';
            testLead.CountryCode = 'US';
            testLead.City = 'Dallas';
            //testLead.State='VIC';
            testLead.Postalcode = '2060';
            // testLead.FirstName='Joihn1';
            // testLead.LastName='Smiith1';
            testLead.Salutation = 'Mr.';
            testLead.Email = 'abc453422@xyz.com';
            testLead.Phone = '123456';
            testLead.Frequent_Flyer_Number__c = '64322';
            testLead.Business_Type__c = 'Agency';
            testLead.Job_Role__c = 'Agency Manager';
            testLead.Function__c = 'Finance';
            testLead.IATA__c = '41373';
            testLead.Agency_TIDS__c = '41373';
            testLead.Account_Tier__c = 'Industry - Gold';
            testLead.Estimated_Total_Air_Travel_Spend__c = 110;
            testLead.NumberOfEmployees = 100;
            testLead.AnnualRevenue = 1000;
            testLead.LeadSource = 'Web';
            testLEad.Industry = 'Manufacturing';
            testLead.Website = 'www.test.com';
            testLead.MobilePhone = '9999999';
            testLead.Agency_Sales_Region__c = sr.id;
            testLead.Parent_Account__c = acc.id;

            Test.startTest();
            insert testLead;

            leadId = testLead.Id;
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(LeadId);

            LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted = true limit 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);

            //   Database.LeadConvertResult lcr = Database.convertLead(lc);
            //   System.assert(lcr.isSuccess());
            Test.stopTest();

        }

    }
    static testMethod void Test_LeadConvertForOtherAcc() {
        LEAD_COUNT += 1;

        Profile pf = [Select Id from Profile where Name = 'System Administrator'];
        User u = new User();
        u.FirstName = 'Test';
        u.LastName = 'User';
        u.Email = 'testuser@test123456789.com';
        u.CompanyName = 'test.com';
        u.Title = 'Test User';
        u.Username = 'testuser@test123456789.com';
        u.Alias = 'testuser';
        u.CommunityNickname = 'Test User';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = pf.Id;
        u.LanguageLocaleKey = 'en_US';
        insert u;

        system.runAs(u) {

            // TestUtilityDataClassQantas.enableTriggers();
            String prospectRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
            Account acc = new Account(Name = 'Sample', Active__c = true, Aquire__c = true, Type = 'Prospect Account', RecordTypeId = prospectRecordTypeId, Migrated__c = false, Estimated_Total_Air_Travel_Spend__c = 0, Manual_Revenue_Update__c = false, Agency__c = 'N', Dealing_Flag__c = 'N', Aquire_Override__c = 'N', Contract_End_Date__c = Date.Today());

            insert acc;
            Sales_Region__c sr = new Sales_Region__c(Name = 'AD - Agency Development Account', sales_h_eq_type__c = 'Industry', sales_h_type__c = 'Industry');
            insert sr;

            Lead testLead = new Lead();
            testLead.FirstName = 'Test First1' + LEAD_COUNT;
            testLead.LastName = 'Test Last1' + LEAD_COUNT;
            testLead.Company = 'Test Co';
            testLead.ABN_Tax_Reference__c = '23232323231';
            testLead.Status = 'Qualified';
            testLead.Company = 'Appshark';
            testLead.Account_Tier__c = 'Industry - Platinum';
            testLead.Agency_TIDS__c = '43991';
            testLead.Business_Type__c = 'Agency';
            testLead.Estimated_Total_Air_Travel_Spend__c = 100;
            testLead.Frequent_Flyer_Number__c = '123';
            testLead.Function__c = 'Finance';
            testLead.IATA__c = '8764';
            testLead.Job_Role__c = 'Agency Owner';
            testLead.Job_Title__c = 'Mr';
            testLead.Legal_Name__c = 'Test ABC';
            testLead.New_Account_Type__c = 'Other';
            testLead.Data_Quality_Score_for_All__c = 100;
            testLead.Street = 'Test';
            testLead.CountryCode = 'US';
            testLead.City = 'Dallas';
            //testLead.State='VIC';
            testLead.Postalcode = '2060';
            // testLead.FirstName='Joihn1';
            // testLead.LastName='Smiith1';
            testLead.Salutation = 'Mr.';
            testLead.Email = 'abc453422@xyz.com';
            testLead.Phone = '123456';
            testLead.Frequent_Flyer_Number__c = '64322';
            testLead.Business_Type__c = 'Agency';
            testLead.Job_Role__c = 'Agency Manager';
            testLead.Function__c = 'Finance';
            testLead.IATA__c = '41373';
            testLead.Agency_TIDS__c = '41373';
            testLead.Account_Tier__c = 'Industry - Gold';
            testLead.Estimated_Total_Air_Travel_Spend__c = 110;
            testLead.NumberOfEmployees = 100;
            testLead.AnnualRevenue = 1000;
            testLead.LeadSource = 'Web';
            testLEad.Industry = 'Manufacturing';
            testLead.Website = 'www.test.com';
            testLead.MobilePhone = '9999999';
            testLead.Agency_Sales_Region__c = sr.id;
            testLead.Parent_Account__c = acc.id;

            Test.startTest();
            insert testLead;

            leadId = testLead.Id;
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(LeadId);

            LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted = true limit 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);

            //   Database.LeadConvertResult lcr = Database.convertLead(lc);
            //   System.assert(lcr.isSuccess());
            Test.stopTest();

        }

    }
}