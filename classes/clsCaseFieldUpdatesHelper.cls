public class clsCaseFieldUpdatesHelper{
   
    public static void generateAuthorityNumber(List<Case> caseList){
        try{
            QIC_AutoNumber__c qicInfo = QIC_AutoNumber__c.getValues('Fee Waiver Request'); 
            Decimal authNum = qicInfo.LastNumber__c;   
            String blanketWaiverType = qicInfo.CaseType__c ;
            String QICRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(qicInfo.Record_Type__c).getRecordTypeId();
            String blanketWaiverRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(qicInfo.Blanket_Waiver__c).getRecordTypeId();
            
            for(Case c: caseList){
                if(c.Authority_Number__c == null){
                if(c.RecordTypeId == blanketWaiverRecordTypeId  && c.Status == qicInfo.ClosedStatus__c ){
                    c.Authority_Number__c = authNum.format().replace(',','') ;
                    authNum = authNum + 1; 
                    if(authNum == qicInfo.EndNumber__c){
                        authNum = qicInfo.StartNumber__c;
                    }
                }
                if(c.RecordTypeId == QICRecordTypeId  && c.Status == qicInfo.ClosedStatus__c && c.Type != blanketWaiverType){
                    c.Authority_Number__c = authNum.format().replace(',','') ;
                    authNum = authNum + 1; 
                    if(authNum == qicInfo.EndNumber__c){
                        authNum = qicInfo.StartNumber__c;
                    }
                }
                }
            }
            if(qicInfo.LastNumber__c != authNum){
                qicInfo.LastNumber__c = authNum;
                update qicInfo ;
            } 
        }catch(Exception e){
            system.debug('EXCEPTION_Authority_Number:'+e.getStackTraceString());
        }
    }
}