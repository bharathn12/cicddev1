/**********************************************************************************************************************************
 
    Created Date    : 24/04/17 (DD/MM/YYYY)
    Added By        : Karpagam Swaminathan
    Description     : Transitioning the Customer from a QBS to QBR for the non-renewed customers
    JIRA            : CRM-2522
    Version         : V1.0 - 24/04/17 - Initial version
 
**********************************************************************************************************************************/




global class CustTransitionQBStoQBR_Batch implements Database.Batchable < sObject > {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([Select Id, Type, AccountID from Case Where Type = 'Deal Termination' and Status = 'Closed - Deal Implemented' and Closeddate = Today]);
    }

    global void execute(Database.BatchableContext ctx, List < Case > caseList) {
        List < Contact > conToUpdate = new List < Contact > ();
        List < Campaign > camToUpdate = new List < Campaign > ();
        List < CampaignMember > cammemToUpdate = new List < CampaignMember > ();
        List < Account > activeAccounts = new List < Account > ();
        // List<AccountContactRelations> acrList = new List<AccountContactRelations>();
        Map < ID, ID > caseAccountMap = new Map < ID, ID > ();
        Map < ID, List < Contact >> conAccountMap = new Map < ID, List < Contact >> ();

        // Define a Map to hold the Account and Case Mapping which satisfying below query
        system.debug('caseList**********' + caseList);
        for (Case c: caseList)
            caseAccountMap.put(c.AccountId, c.Id);
        system.debug('caseAccountMap%%%%%%%%%%%' + caseAccountMap);
        //Reterive the Accounts which has Contract expired in past month        
        activeAccounts = [SELECT Id, Name, Contract_End_Date__c FROM Account WHERE ID IN: caseAccountMap.keyset() and Contract_End_Date__c = LAST_N_DAYS: 30];

        //If the above Map is Not emapty
        if (!caseAccountMap.isEmpty()) {
            //Look for a contact with Job Role "Travel Co-Ordinator/Booker"
            for (Contact con: [SELECT ID, AccountId FROM Contact Where AccountID IN: caseAccountMap.keyset() and Job_Role__c = 'Travel Co-Ordinator/Booker']) {
                conToUpdate.add(con);
            }

            //Get the id of the campaign "QBS to QBR Transition"
            camToUpdate = [Select Id FROM Campaign WHERE Name =: System.Label.QBS_to_QBR_Transition and EndDate = THIS_YEAR];

            if (conToUpdate.size() == 0) {
                //most recent updated contact from the account which has a phone and email. 
                for (Contact con: [SELECT ID FROM Contact Where AccountID IN: caseAccountMap.keyset() and Phone != Null AND Email != Null ORDER BY LastModifiedDate DESC Limit 1]) {
                    conToUpdate.add(con);
                }

            }
            system.debug('conToUpdate###########' + conToUpdate);
            if (conToUpdate.size() > 0) {
                //Add the contact to the campaign named as "QBS to QBR Transition"  

                for (Contact c: conToUpdate) {
                    CampaignMember cm = New CampaignMember(CampaignId = camToUpdate[0].Id, ContactId = c.Id, Status = 'Planned');
                    cammemToUpdate.add(cm);
                }
            }
        }


        try {
            Boolean sucessFlag;
            Set < ID > CamMemID = new Set < ID > ();
            Database.SaveResult[] srListInsert = Database.Insert(cammemToUpdate, false);
            for (Database.SaveResult sr: srListInsert) {
                if (sr.isSuccess()) {
                    sucessFlag = TRUE;
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully Inserted QBS to QBR Transition:' + sr.getId());
                    CamMemID.add(sr.getId());

                } else {
                    // Operation failed, so get all errors                
                    for (Database.Error err: sr.getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('QBS to QBR Transition that affected this error: ' + err.getFields());
                    }
                }
            }
            if (sucessFlag == TRUE) {
                List < CampaignMember > CM = [select contact.id, contact.AccountId, contact.Name, campaignId, contact.Owner.Name from campaignMember where contactId != ''
                    and Id IN: CamMemID
                ];
                Set < ID > AccountIdSet = new Set < ID > ();
                Set < ID > ContactIdSet = new Set < ID > ();
               // List < CollaborationGroup > GroupId = [SELECT Id FROM CollaborationGroup WHERE Name = 'QBS To QBR Transition' limit 1 ];
              //  String GroupIDs = GroupId[0].Id;
                for (CampaignMember c: CM) {
                    AccountIdSet.add(c.contact.AccountId);
                    ContactIdSet.add(c.contact.id);
                    //Posting Chatter Feed on Account Object
                    ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                    ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                    ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                    ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
                    ConnectApi.TextSegmentInput textSegmentInput1 = new ConnectApi.TextSegmentInput();
                    messageBodyInput.messageSegments = new List < ConnectApi.MessageSegmentInput > ();
                    
                    textSegmentInput1.text ='Hi ';
                    messageBodyInput.messageSegments.add(textSegmentInput1);
                    mentionSegmentInput.id = Label.QBS_to_QBR_GroupID;
                    messageBodyInput.messageSegments.add(mentionSegmentInput);

                    textSegmentInput.text = 'The contact ' + c.contact.Name + ' has been added as a campaign member to the campaign' + ' QBS to QBR' + ' for the QBS to QBR transition.' + '\n\nThanks,\n' + c.contact.owner.Name;
                    messageBodyInput.messageSegments.add(textSegmentInput);

                    feedItemInput.body = messageBodyInput;
                    feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                    feedItemInput.subjectId = c.contact.AccountId;


                    ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);

                }
            }

        } catch (Exception e) {
            System.debug('Exception Occured: ' + e.getMessage());
        }
    }

    global void finish(Database.BatchableContext ctx) {
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id =: ctx.getJobId()];
        String UserEmail = Label.CustTransitionQBStoQBR_Batch_Email;
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {
            UserEmail
        });
        mail.setReplyTo(UserEmail);
        mail.setSenderDisplayName('Batch Processing');
        mail.setSubject('Batch Process Completed');
        mail.setPlainTextBody('Batch Process has completed');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
            mail
        });
    }
}