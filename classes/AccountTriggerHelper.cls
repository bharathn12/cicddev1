/*----------------------------------------------------------------------------------------------------------------------
Author:        Praveen Sampath
Company:       Capgemini
Description:   Class to be invoked from Account Trigger
Inputs:
Test Class:    AccountTriggerHelperTest. 
************************************************************************************************
History
************************************************************************************************
06-Mar-2017    Benazir S A               Initial Design
07-Mar-2017    Praveen Sampath          Added createDelAccountLogTracker Method
-----------------------------------------------------------------------------------------------------------------------*/
public class AccountTriggerHelper {
    
    /*--------------------------------------------------------------------------------------      
	Method Name:        updateAccountTeam
	Description:        Method to Assign & create Account team members based on user department
	Parameter:          Account New Map & Old Map 
	--------------------------------------------------------------------------------------*/    
    public static void updateAccountTeam(Map<Id,Account> newMapAccount, Map<Id,Account> oldMapAccount){
    	try{
    		AccountTeamHandler accHandler = new AccountTeamHandler();
    		List<Account> lstNewAccount = newMapAccount.values();
            accHandler.setRevenueManualUpdate(lstNewAccount, oldMapAccount);
            accHandler.createAccountTeamMembers(lstNewAccount, oldMapAccount);
    	}
    	catch(Exception ex){
    		String recType = CustomSettingsUtilities.getConfigDataMap('Log Exception Logs Rec Type');    
            CreateLogs.LogWrapper logWrap = new CreateLogs.LogWrapper('Account Team - Account Trigger', 'AccountTriggerHelper', 
                                                                      'updateAccountTeam', UserInfo.getUserName(), '', 
                                                                      '', false, recType);
            
            Log__c objLogs = CreateLogs.createLogRec(logWrap, '');
            CreateLogs.createApplicationLog(objLogs, ex);
    	}
    }

    /*--------------------------------------------------------------------------------------      
	Method Name:        createAccountEligiblityLogs
	Description:        Method to Create AccountIneligibilty Logs and Update
	Parameter:          Account New Map & Old Map 
	--------------------------------------------------------------------------------------*/    
    public static void createAccountEligiblityLogs(Map<Id,Account> newMapAccount, Map<Id,Account> oldMapAccount){
    	try{
    		AccountEligibilityLogHandler.createAccountEligiblityLogs(newMapAccount, oldMapAccount);
    	}
    	catch(Exception ex){
    		String recType = CustomSettingsUtilities.getConfigDataMap('Log Exception Logs Rec Type');    
            CreateLogs.LogWrapper logWrap = new CreateLogs.LogWrapper('SME - Account Trigger', 'AccountTriggerHelper', 
                                                                      'createAccountEligiblityLogs', UserInfo.getUserName(), '', 
                                                                      '', false, recType);
            
            Log__c objLogs = CreateLogs.createLogRec(logWrap, '');
            CreateLogs.createApplicationLog(objLogs, ex);
    	}
    }

    /*--------------------------------------------------------------------------------------      
	Method Name:        calculateDiscountCode
	Description:        Method to DiscountCode Calculations on Products
	Parameter:          Account New Map & Old Map 
	--------------------------------------------------------------------------------------*/    
    public static void calculateDiscountCode(Map<Id,Account> newMapAccount, Map<Id,Account> oldMapAccount){
		try{
            AccountAirlineLevelHandler.updateDiscountCodeOnProducts(newMapAccount, oldMapAccount);
    	}
    	catch(Exception ex){
    		String recType = CustomSettingsUtilities.getConfigDataMap('Log Exception Logs Rec Type');    
            CreateLogs.LogWrapper logWrap = new CreateLogs.LogWrapper('SME - Account Trigger', 'AccountTriggerHelper', 
                                                                      'calculateDiscountCode', UserInfo.getUserName(), '', 
                                                                      '', false, recType);
            
            Log__c objLogs = CreateLogs.createLogRec(logWrap, '');
            CreateLogs.createApplicationLog(objLogs, ex);
    	}
    }

    /*--------------------------------------------------------------------------------------      
	Method Name:        createDelAccountLogTracker
	Description:        Method to Create Delete Tracker Logs records when any Account is Deleted
	Parameter:          Account Old Map 
	--------------------------------------------------------------------------------------*/    
    public static void createDelAccountLogTracker(Map<Id,Account> oldMapAccount){
        try{
            String recTypeName = CustomSettingsUtilities.getConfigDataMap('Del Log SME Account Rec Type');
            List<CreateLogs.DeleteLogWrapper> lstDelLogWrapper = new List<CreateLogs.DeleteLogWrapper>();
            for(Account acc: oldMapAccount.values()){
                String directvalue = CustomSettingsUtilities.getConfigDataMap('AccListner Type Direct Stream');
                if(acc.Listner_Type__c == directvalue){
                    CreateLogs.DeleteLogWrapper delLogWrapper = new CreateLogs.DeleteLogWrapper();
                    delLogWrapper.recType = recTypeName;
                    delLogWrapper.recName =  acc.Name;
                    delLogWrapper.abnNumber = acc.ABN_Tax_Reference__c;
                    delLogWrapper.sobjectId = acc.Id;
                    delLogWrapper.sobjectName = 'Account';
                    lstDelLogWrapper.add(delLogWrapper);
                }
            }
            List<Delete_Tracker_Log__c> lstDelTrackerLog = CreateLogs.createDeleteLog(lstDelLogWrapper);
            insert lstDelTrackerLog;
        }catch(Exception ex){
            String recType = CustomSettingsUtilities.getConfigDataMap('Log Exception Logs Rec Type');    
            CreateLogs.LogWrapper logWrap = new CreateLogs.LogWrapper('Direct Stream - Account Trigger', 'AccountTriggerHelper', 
                                                                      'createDelAccountLogTracker', UserInfo.getUserName(), '', 
                                                                      '', false, recType);
            
            Log__c objLogs = CreateLogs.createLogRec(logWrap, '');
            CreateLogs.createApplicationLog(objLogs, ex);
        }
    }

}