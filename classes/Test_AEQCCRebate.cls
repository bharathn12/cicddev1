@isTest 
public with sharing class Test_AEQCCRebate {
        @testsetup
    static void createData(){
        List<QantasConfigData__c> lstConfigData = new List<QantasConfigData__c>();
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Exception Logs Rec Type','Exception Logs'));
        insert lstConfigData;
    }
    @isTest      
    public static void Test_AEQCCRebate() {
    TestUtilityDataClassQantas.enableTriggers();
        Profile pf = [Select Id from Profile where Name = 'System Administrator'];
        User u = new User();
        u.FirstName = 'Test';
        u.LastName = 'User';
        u.Email = 'testuser@test123456789.com';
        u.CompanyName = 'test.com';
        u.Title = 'Test User';
        u.Username = 'testuser@test123456789.com';
        u.Alias = 'testuser';
        u.CommunityNickname = 'Test User';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = pf.Id;
        u.LanguageLocaleKey = 'en_US';
        insert u;
     system.runAs(u){ 
       String prospectRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
       Account acc = new Account(Name = 'Sample', Active__c = true, Aquire__c = true,Type = 'Prospect Account', 
                                      RecordTypeId = prospectRecordTypeId, Migrated__c = false, 
                                      Estimated_Total_Air_Travel_Spend__c = 0, Manual_Revenue_Update__c = false,
                                      Agency__c = 'N', Dealing_Flag__c = 'N', Aquire_Override__c = 'N' ,Contract_End_Date__c = Date.Today()
                                      );
                                    
                                    
       insert acc;
        AEQCCRebate controller = new AEQCCRebate(new ApexPages.StandardController(new Product_Registration__c()));

        List<Product_Registration__c> Prodreglist = new List<Product_Registration__c>();
        List<AEQCC_Rebates__c> Reblist = New List<AEQCC_Rebates__c>();
        Product_Registration__c Pr = new Product_Registration__c(name = 'Test AEQCC REBATE',Account__c=acc.id);
        AEQCC_Rebates__c Aeqreb = new AEQCC_Rebates__c (Rebate_Year__c = '2015',Rebate_Tier__c ='2 %',Rebate_term__c='Jul-Dec',Rebate_Payment__c=753,Cheque_Amount__c=826.61,Rebate_Sent_to__c='Adam Simon Po Box 103 Port Melbourne Vic 3207',Prod_Reg_Link__c='a2cO00000013Djs');
        
        //Custom Setting
        TestUtilityDataClassQantas.getrelatedCustObjRecs1();
        
     try{
            Database.Insert(Pr);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }

    try{
            Database.Insert(Aeqreb);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }  
 
        AEQCCRebate controllerTest = new AEQCCRebate(new ApexPages.StandardController(pr));     
        controllerTest.getrelatedCustObjRecs1();
        Reblist.add(Aeqreb);
   }     
 }    
}