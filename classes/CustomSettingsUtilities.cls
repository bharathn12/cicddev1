/*----------------------------------------------------------------------- 
File name:      CustomSettingsUtilities.cls
Author:         Praveen Sampath
Company:        Capgemini  
Description:    This class is created as Utill class for Custom Settings.
Test Class:     
Created Date :  28/10/2016
History
<Date>      <Authors Name>     <Brief Description of Change>
-----------------------------------------------------------------------*/
public class CustomSettingsUtilities{
     
    //Returns an individual record value from QantasConfigData__c custom setting. If the record is not found for the input parameters, it returns null.
    public static String getConfigDataMap(String paramName){
            if (paramName != ''){
                if(QantasConfigData__c.getValues(paramName)!=null){
                    QantasConfigData__c configData = QantasConfigData__c.getValues(paramName);
                    string confVal = configData.Config_Value__c;
                    return confVal;
                }
            }           
        return ''; 
    }
}