/*******************************************************************************************************************************
Description: This apex is invoked by a process builder which will convert lead into Account and Contact in 
case of no duplicates exist. Update account type accordingly.
Created Date: 10-04-2017
JIRA: CRM-2545
******************************************************************************************************************************** */

Public class AutoConvertLeads {@InvocableMethod
    public static void LeadAssign(List < Id > LeadIds) {
        try {
            Map < String,
            Contact > conMap = new Map < String,
            Contact > ();
            Contact[] consToUpdate = new List < Contact > ();
            String msg = '';
            String ErrorMessageRecipient = '';
            Set < String > emailSet = new Set < String > ();
            List < Lead > newList = new List < Lead > ();
            Id accountId;
            Id contactId;
            Id leadId;

            newList = [Select Email from Lead where Id IN: LeadIds];

            //Get email addresses.
            for (Lead l: newList) {
                emailSet.add(l.Email);
            }

            //Get existing contacts.        
            for (Contact con: [select Email from Contact where email in :emailSet]) {
                conMap.put(con.Email, con);
            }
            LeadStatus CLeadStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = true Limit 1];
            List < Database.LeadConvert > MassLeadconvert = new List < Database.LeadConvert > ();
            List < String > convertMsgs = new List < String > (); // List of Strings containing status messages about each conversion operation
            for (Lead l: newList) {
                Database.LeadConvert Leadconvert = new Database.LeadConvert();
                Leadconvert.setLeadId(l.id);
                Leadconvert.setConvertedStatus(CLeadStatus.MasterLabel);
                Leadconvert.setDoNotCreateOpportunity(TRUE); //Remove this line if you want to create an opportunity from Lead Conversion 
                Leadconvert.setSendNotificationEmail(TRUE);
                if (conMap.keySet().contains(l.Email)) {
                    Leadconvert.setContactId(conMap.get(l.Email).Id);
                }
                MassLeadconvert.add(Leadconvert);
            }

            if (!MassLeadconvert.isEmpty()) {
                List < Database.LeadConvertResult > lcr = Database.convertLead(MassLeadconvert, false);

                for (Database.LeadConvertResult lcrList: lcr) {
                    //Capture failed conversions for the email message.
                    Lead le = [SELECT Id, Name, OwnerId,Auto_Lead_Convert__c from Lead WHERE ID = :lcrList.getLeadId()];
                    if (lcrList.isSuccess()) {
                        accountId = lcrList.getAccountId();
                        contactId = lcrList.getContactId();
                        leadId = lcrList.getLeadId();
                        Account acc = [SELECT Name From Account WHERE ID = :accountId];
                        Contact con = [SELECT Name From Contact WHERE ID = :contactId];
                        system.debug('leadId**********' + leadId);
                        system.debug('accountId**********' + accountId);
                        //String convertMsg='CONVERSION SUCCEEDED: Converted Lead: ' + le.Name + ' into Account: ' + acc.Name + ' and Contact: ' + con.Name + '\n';
                        String convertMsg = 'Account:' + ' <' + acc.Name + '>' + ' and Contact:' + ' <' + con.Name + '>' + ' created from your Lead. Thank you.';

                        convertMsgs.add(convertMsg);
                        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

                        messageBodyInput.messageSegments = new List < ConnectApi.MessageSegmentInput > ();

                        mentionSegmentInput.id = le.OwnerId;
                        messageBodyInput.messageSegments.add(mentionSegmentInput);

                        textSegmentInput.text = ' ' + convertMsg;
                        messageBodyInput.messageSegments.add(textSegmentInput);

                        feedItemInput.body = messageBodyInput;
                        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                        feedItemInput.subjectId = accountId;

                        ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
                    }
                    else {
                        system.debug('inside lead error part*************');
                        le.Auto_Lead_Convert__c=False;
                        update le;
                        Database.Error[] errors = lcrlist.getErrors();
                        String convertMsg = 'Case has been created for the System Administrator to action. Thank you.';
                        for (Database.Error err: errors) {
                            //  convertMsg += '\tERROR: ' + err.getMessage() + '\n';
                            system.debug('convertMsg ############' + convertMsg);
                            convertMsgs.add(convertMsg);
                            //ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), le.Id, ConnectApi.FeedElementType.FeedItem, 'Hi'+convertMsgs);               

                            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

                            messageBodyInput.messageSegments = new List < ConnectApi.MessageSegmentInput > ();

                            mentionSegmentInput.id = le.OwnerId;
                            messageBodyInput.messageSegments.add(mentionSegmentInput);

                            textSegmentInput.text = ' ' + convertMsg;
                            messageBodyInput.messageSegments.add(textSegmentInput);

                            feedItemInput.body = messageBodyInput;
                            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                            feedItemInput.subjectId = le.Id;

                            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
                        }

                    }
                    if (accountId != null) {
                        system.debug('accountId************' + accountId);
                        List < account > accToUpdate = new List < account > ();
                        Map < String,
                        ID > RecordTypeMAP = new Map < String,
                        Id > ();
                        for (RecordType RecType: [Select Id, DeveloperName from RecordType where sObjectType = 'account']) {
                            RecordTypeMAP.put(RecType.DeveloperName, RecType.id);
                        }
                        List < Account > aclist = [SELECT Id, Name, New_Account_Type__c, Type, IATA_Number__c, RecordTypeID, BillingCountry, BillingCountryCode, BillingState FROM Account WHERE Id = :accountId];
                        For(account accRec: aclist) {
                            system.debug('accRec.BillingCountry########' + accRec.BillingCountry);
                            system.debug('accRec.BillingCountryCode########' + accRec.BillingCountryCode);
                            system.debug('accRec.BillingState########' + accRec.BillingState);
                            if (accRec.New_Account_Type__c == 'Agency') {
                                system.debug('accRec*************' + accRec);
                                accRec.Type = accRec.New_Account_Type__c;
                                accRec.Qantas_Industry_Centre_ID__c = accRec.IATA_Number__c;
                                accRec.RecordTypeID = RecordTypeMAP.get('Agency_Account');
                                accRec.Industry = 'Travel';

                                if (accRec.BillingCountry == 'AU') {
                                    system.debug('accRec.BillingState**********' + accRec.BillingState);
                                    String myString1 = accRec.Name;
                                    system.debug('myString1%%%%%%' + myString1);
                                    String myString2 = accRec.BillingState;
                                    system.debug('myString2 $$$$$$$' + myString2);
                                    Boolean result = myString1.contains(myString2);
                                    system.debug('result ^^^^^^^^^' + result);
                                    if (result == false) accRec.Name = accRec.Name + ' [' + accRec.BillingState + ']';
                                }
                                else {
                                    system.debug('accRec.BillingCountryCode**********' + accRec.BillingCountryCode);
                                    String myString1 = accRec.Name;
                                    system.debug('myString1%%%%%%' + myString1);
                                    String myString2 = accRec.BillingCountryCode;
                                    system.debug('myString2 $$$$$$$' + myString2);
                                    Boolean result = myString1.contains(myString2);
                                    system.debug('result ^^^^^^^^^' + result);
                                    if (result == false) accRec.Name = accRec.Name + ' [' + accRec.BillingCountryCode + ']';
                                }
                                accToUpdate.add(accRec);
                            }
                            else if (accRec.New_Account_Type__c == 'Prospect') {
                                accRec.Type = accRec.New_Account_Type__c;
                                accRec.RecordTypeid = RecordTypeMAP.get('Prospect_Account');
                                accToUpdate.add(accRec);
                            }

                            else if (accRec.New_Account_Type__c == 'Customer') {
                                accRec.Type = accRec.New_Account_Type__c;
                                accRec.RecordTypeid = RecordTypeMAP.get('Customer_Account');
                                accToUpdate.add(accRec);
                            }

                            else if (accRec.New_Account_Type__c == 'Charter') {
                                accRec.Type = accRec.New_Account_Type__c;
                                accRec.RecordTypeid = RecordTypeMAP.get('Charter_Account');
                                accToUpdate.add(accRec);
                            }
                            else if (accRec.New_Account_Type__c == 'Other') {
                                accRec.Type = accRec.New_Account_Type__c;
                                accRec.RecordTypeid = RecordTypeMAP.get('Other_Account');
                                accToUpdate.add(accRec);
                            }

                        }
                        if (accToUpdate.size() > 0) update accToUpdate;
                    }
                }
            }
            //Send email with failed conversions.
            /*  if(msg != '') {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[]{ErrorMessageRecipient,'karpagam.swaminathan@tcs.com'};
            
            mail.setToAddresses(toAddresses);
            mail.setSubject('Lead conversion errors');
            mail.setPlainTextBody(msg);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });           
        }*/
            // Send notification email
            String msgBody = 'Automatic Lead conversion results:\n';
            for (String convertMsg: convertMsgs) {
                msgBody += convertMsg;
            }
            // sendEmail(UserInfo.getUserId(), UserInfo.getName(), 'Automatic Lead Conversion', msgBody, null);
        }
        catch(Exception ex) {
            // sendEmail(UserInfo.getUserId(), UserInfo.getName(), 'Automatic Lead Conversion Failed', ex.getMessage(), null);
            throw ex;
        }

    }
    /* public static Boolean sendEmail(Id targetId, String senderDisplayName, 
        String subject, String textBody, Messaging.EmailFileAttachment[] attachments) {
        
        // Create and send a single email message to the targetId      
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        
        // Use the specified template
        email.setTargetObjectId(targetId);
        email.setSubject(subject);
        email.setPlainTextBody(textBody);
        email.setSenderDisplayName(senderDisplayName);
        email.setSaveAsActivity(false);
        if (attachments != null) {
            email.setFileAttachments(attachments);
        }
        Messaging.SendEmailResult[] results = 
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        return results[0].isSuccess();        
    } */
}