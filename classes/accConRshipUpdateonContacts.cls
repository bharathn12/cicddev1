/*-----------------------------------------------------------------------------------------------------------------------------
Author:        
Company:       
Description:   
Test Class:   
*********************************************************************************************************************************
*********************************************************************************************************************************
History
Abhijeet P /Jayaraman J          For Freight to populate citycode in ACR record added few code        
********************************************************************************************************************************/

Public class accConRshipUpdateonContacts {

    public static void AccContactInsUpdate(list<Contact> listOfCon) {
    
    set<Id> ConIds = new set<Id>();
    map<Id, Contact> mapContact = new map<Id, Contact>();
    list<AccountContactRelation> listOfAccConRel = new list<AccountContactRelation>();
    //Freight change Start
    set<Id> AirportcodeIds = new set<Id>();
    list<Airport_Code__c> listOfAirportcode = new list<Airport_Code__c>();
    map<Id, Airport_Code__c> mapAirportcode = new map<Id, Airport_Code__c>();
    //Freight change End
    
    for(Contact Cont : listOfCon) {
        ConIds.add(Cont.Id);
        mapContact.put(Cont.Id, Cont);
        //Freight change
        AirportcodeIds.add(Cont.Freight_Contact_City__c);
    }
    
     //Freight change Start
    listOfAirportcode =[Select Id,Name from Airport_Code__c where id In :AirportcodeIds ];
    
    if(listOfAirportcode.size() > 0)
    {
        for(Airport_Code__c aico : listOfAirportcode) {
            mapAirportcode.put(aico.id, aico);
        }
    }
     //Freight change End
    
    listOfAccConRel = [SELECT Function__c,Business_Type__c,Job_Role__c,ContactId,IsDirect, Freight_City__c FROM AccountContactRelation WHERE ContactId IN : ConIds];
    system.debug('***SOQLUPDATEACR***'+ listOfAccConRel ); // City__c
    
    if(listOfAccConRel.size() > 0 && listOfAccConRel.size() <= 1){
       system.debug('***SizeofACR***'+ listOfAccConRel.size());
       
        for(AccountContactRelation accCon : listOfAccConRel) {
            accCon.Function__c = mapContact.get(accCon.ContactId).Function__c;
            accCon.Business_Type__c = mapContact.get(accCon.ContactId).Business_Types__c;  
            accCon.Job_Role__c = mapContact.get(accCon.ContactId).Job_Role__c;
            system.debug('***ACR***'+ accCon.Job_Role__c);
            acccon.Job_Title__c = mapContact.get(accCon.ContactId).Job_Title__c;
            acccon.Related_Phone__c = mapContact.get(accCon.ContactId).Phone;
            //Freight change Start
            if(mapAirportcode.size() > 0)
             {
              acccon.Freight_City__c = mapAirportcode.get(mapContact.get(accCon.ContactId).Freight_Contact_City__c).Name;
             }
             //Freight change End
            acccon.Related_Email__c = mapContact.get(accCon.ContactId).Email;                    
         }
            Database.update(listOfAccConRel);
            system.debug('***UPDATEACR***'+ listOfAccConRel); 
      } 
   }
}