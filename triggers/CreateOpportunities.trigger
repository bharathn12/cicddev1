/**********************************************************************************************************************************
 
    Created Date: 09/03/2015
 
    Description: Process Product Registration Trigger For:
                 1. Opportunity creation based on travel frequency before insert.
  
    Versión:
    V1.0 - 09/03/2015 - Initial version [FO]
 
**********************************************************************************************************************************/
trigger CreateOpportunities on Product_Registration__c (after insert, after update, before insert, before update) {
    
    try{    
        Trigger_Status__c ts = Trigger_Status__c.getValues('CreateOpportunities');
        if(ts.Active__c){
            
           ProductRegistrationHandler PRH = new ProductRegistrationHandler();
           if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){    
                PRH.processAccountAndOpportunity(Trigger.new);
           }     
           if(Trigger.isBefore){  
                if(Trigger.isInsert){
                    PRH.processAMEXEligibility(Trigger.new);
                }
                if(Trigger.isUpdate){
                    PRH.processAMEXEligibility(Trigger.new, Trigger.oldMap);
                }
           }
        }
    }catch(Exception e){
        System.debug('Error Occured From Product Registration Trigger: '+e.getMessage());
    }    
}