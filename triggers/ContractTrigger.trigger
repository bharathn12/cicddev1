/**********************************************************************************************************************************

Description: Based on the Contract Start Date & End Date, we are firing Time based workflows to flag on the Contracted flag.
Once the flag on the the Start Date & End Date of the Current live contract will be copied to Account & the count of 
live contracts also maintained on the Account. For this functionality we are using this trigger. 

Change History:
======================================================================================================================
Name                    Jira        Description                                                 Tag
======================================================================================================================
Gourav Bhardwaj         1821        Add functionality to capture Corporate Deal Flag            T01
                                    Start/End Dates change history in Eligibility Log

**********************************************************************************************************************************/
trigger ContractTrigger on Contract__c(After Update,After Insert) {    
    try{
        Trigger_Status__c ts = Trigger_Status__c.getValues('ContractTrigger');
        if(ts.Active__c){
            
            if(Trigger.isUpdate){

                
                ContractTriggerHandler.ContractfieldsupdateonAccount(Trigger.New, Trigger.oldMap);
                ContractTriggerHandler.contractcommencementdate(Trigger.New, Trigger.oldMap);
                ContractTriggerHandler.contractexpirydate(Trigger.New, Trigger.oldMap);
              
            }  

           
            
        }
        
    }catch(Exception e){
        System.debug('Error Occured From Contract Trigger: '+e.getMessage());
    }
}