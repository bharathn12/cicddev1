<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Cheque_Amount__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Cheque amount is inclusive of GST</inlineHelpText>
        <label>Cheque Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Date_Processed__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Date when the cheque was issued to the customer.</inlineHelpText>
        <label>Date Processed</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Latest_three__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Latest three</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Prod_Reg_Link__c</fullName>
        <externalId>false</externalId>
        <label>Prod Reg Link</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Product_Registration__c.RecordTypeId</field>
                <operation>equals</operation>
                <value>AEQCC Registration</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Product_Registration__c</referenceTo>
        <relationshipLabel>AEQCC Rebates</relationshipLabel>
        <relationshipName>AEQCC_Rebates</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Rebate_Payment__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Rebate Payment is exclusive of GST</inlineHelpText>
        <label>Rebate Payment</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Rebate_Sent_to__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Contact name and address details are source from American Express, based on Company&apos;s last updated Decision Maker</inlineHelpText>
        <label>Rebate Sent to</label>
        <length>800</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Rebate_Tier__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Rebate tier is determined by Domestic and International revenue on Qantas for the period.</inlineHelpText>
        <label>Rebate Tier</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>0 %</fullName>
                    <default>false</default>
                    <label>0 %</label>
                </value>
                <value>
                    <fullName>2 %</fullName>
                    <default>false</default>
                    <label>2 %</label>
                </value>
                <value>
                    <fullName>3 %</fullName>
                    <default>false</default>
                    <label>3 %</label>
                </value>
                <value>
                    <fullName>4 %</fullName>
                    <default>false</default>
                    <label>4 %</label>
                </value>
                <value>
                    <fullName>5 %</fullName>
                    <default>false</default>
                    <label>5 %</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Rebate_Year__c</fullName>
        <externalId>false</externalId>
        <label>Rebate Year</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>2014</fullName>
                    <default>false</default>
                    <label>2014</label>
                </value>
                <value>
                    <fullName>2015</fullName>
                    <default>false</default>
                    <label>2015</label>
                </value>
                <value>
                    <fullName>2016</fullName>
                    <default>false</default>
                    <label>2016</label>
                </value>
                <value>
                    <fullName>2017</fullName>
                    <default>false</default>
                    <label>2017</label>
                </value>
                <value>
                    <fullName>2018</fullName>
                    <default>false</default>
                    <label>2018</label>
                </value>
                <value>
                    <fullName>2019</fullName>
                    <default>false</default>
                    <label>2019</label>
                </value>
                <value>
                    <fullName>2020</fullName>
                    <default>false</default>
                    <label>2020</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Rebate_term__c</fullName>
        <externalId>false</externalId>
        <label>Rebate term</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Jan-Jun</fullName>
                    <default>false</default>
                    <label>Jan-Jun</label>
                </value>
                <value>
                    <fullName>Jul-Dec</fullName>
                    <default>false</default>
                    <label>Jul-Dec</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>AEQCC Rebate</label>
    <nameField>
        <displayFormat>{000000}</displayFormat>
        <label>AEQCC Rebates Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>AEQCC Rebates</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
