<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
    </brand>
    <description>Freight Sales Activities</description>
    <formFactors>Large</formFactors>
    <label>Qantas Freight Sales</label>
    <navType>Standard</navType>
    <tab>standard-home</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-Task</tab>
    <tab>standard-Case</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Feed</tab>
    <uiType>Lightning</uiType>
    <utilityBar>Qantas_Freight_Sales_UtilityBar</utilityBar>
</CustomApplication>
